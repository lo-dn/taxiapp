package tests

import (
	"context"
	"os"
	"taxiapp/config"
	"taxiapp/internal/delivery/http/handler"
	"taxiapp/internal/models"
	"taxiapp/internal/repository"
	"taxiapp/internal/service"
	"taxiapp/pkg/db"
	"testing"

	"github.com/pressly/goose/v3"
	"github.com/stretchr/testify/suite"
)

var cfg *config.Config

func init() {
	cfg = config.NewConfig("../.env")

	cfg.DB.Host = os.Getenv("TEST_DB_HOST")
	cfg.DB.Port = os.Getenv("TEST_DB_PORT")
	cfg.DB.Name = os.Getenv("TEST_DB_NAME")
	cfg.DB.User = os.Getenv("TEST_DB_USER")
	cfg.DB.Password = os.Getenv("TEST_DB_PASSWORD")
}

type APITestSuite struct {
	suite.Suite

	db      *db.MySql
	serv    *models.Server
	repo    *repository.Repository
	service *service.Service
	handler *handler.Handler
}

func TestAPISuite(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	suite.Run(t, new(APITestSuite))
}

func (s *APITestSuite) SetupSuite() {
	conn, err := db.Init(cfg.DB)
	if err != nil {
		s.FailNow("Failed to connect to mySql:", err)
	}
	s.db = conn

	// Set MySQL Dialect for migration
	if err := goose.SetDialect("mysql"); err != nil {
		s.FailNow("goose.SetDialect failed:", err)
	}

	// Up Migrations
	if err := goose.Up(conn.DB.DB, "../migrations"); err != nil {
		s.FailNow("goose.Up migrations failed:", err)
	}

	s.initDeps()
}

func (s *APITestSuite) TearDownSuite() {
	s.db.DB.Close()
}

func (s *APITestSuite) initDeps() {
	ctx := context.Background()

	// Init domain deps
	s.serv = &models.Server{
		DB:   s.db.DB,
		Conf: cfg,
	}

	s.repo, _ = repository.NewRepository(&ctx, s.db.DB, nil)
	s.service = service.NewService(s.repo)
	s.handler = handler.NewHandler(s.service, &cfg.Auth)
}

func TestMain(m *testing.M) {
	rc := m.Run()
	os.Exit(rc)
}

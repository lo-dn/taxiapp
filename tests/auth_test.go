package tests

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"taxiapp/internal/delivery/http/routes"
)

func (s *APITestSuite) Test_1_Register() {
	router := routes.InitRoutes(s.serv, s.handler)
	r := s.Require()

	data := `{"login": "username", "password": "qwerty123"}`

	req, _ := http.NewRequest("POST", "/api/users", strings.NewReader(data))
	req.Header.Set("Content-type", "application/json")

	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)

	r.Equal(http.StatusOK, resp.Result().StatusCode)
}

func (s *APITestSuite) Test_2_Login() {
	router := routes.InitRoutes(s.serv, s.handler)
	r := s.Require()

	data := `{"login": "username", "password": "qwerty123"}`

	req, _ := http.NewRequest("POST", "/api/users/login", strings.NewReader(data))
	req.Header.Set("Content-type", "application/json")

	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)

	r.Equal(http.StatusOK, resp.Result().StatusCode)
}

package config

type HTTPConfig struct {
	Host       string
	Port       string
	ExposePort string
	Mode       string
}

func LoadHTTPConfig() HTTPConfig {
	return HTTPConfig{
		Host:       getEnv("HOST", ""),
		Port:       getEnv("PORT", ""),
		ExposePort: getEnv("EXPOSE_PORT", ""),
		Mode:       getEnv("GIN_MODE", ""),
	}
}

package config

type Queues struct {
	TaxiTrack string
}

type AMPQConfig struct {
	User   string
	Pass   string
	Host   string
	Port   string
	Queues Queues
}

func LoadAMPQConfig() AMPQConfig {
	return AMPQConfig{
		User: getEnv("RABBIT_USER", ""),
		Pass: getEnv("RABBIT_PASS", ""),
		Host: getEnv("RABBIT_HOST", ""),
		Port: getEnv("RABBIT_PORT", ""),
		Queues: Queues{
			TaxiTrack: getEnv("QUEUE_TAXI_TRACK", ""),
		},
	}
}

package config

import (
	"log"

	"github.com/joho/godotenv"
)

type Config struct {
	Auth    AuthConfig
	DB      DBConfig
	HTTP    HTTPConfig
	AMPQ    AMPQConfig
	Redis   RedisConfig
	Elastic ElasticConfig
}

func NewConfig(pathName string) *Config {
	err := godotenv.Load(pathName)
	if err != nil {
		log.Println("Error loading .env file")
	}

	return &Config{
		Auth:    LoadAuthConfig(),
		DB:      LoadDBConfig(),
		HTTP:    LoadHTTPConfig(),
		AMPQ:    LoadAMPQConfig(),
		Redis:   LoadRedisConfig(),
		Elastic: LoadElasticConfig(),
	}
}

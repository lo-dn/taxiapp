package config

type DBConfig struct {
	User     string
	Password string
	Name     string
	Host     string
	Port     string
}

func LoadDBConfig() DBConfig {
	return DBConfig{
		User:     getEnv("DB_USER", ""),
		Password: getEnv("DB_PASSWORD", ""),
		Name:     getEnv("DB_NAME", ""),
		Host:     getEnv("DB_HOST", ""),
		Port:     getEnv("DB_PORT", ""),
	}
}

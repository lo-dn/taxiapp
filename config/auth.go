package config

type AuthConfig struct {
	AccessSecret string
	ExpAccTime   int

	RefreshSecret string
	ExpRefTime    int
}

func LoadAuthConfig() AuthConfig {
	expAccTime := getEnvAsInt("ACCESS_EXPIRATION_TIME", 30)
	expRefTime := getEnvAsInt("REFRESH_EXPIRATION_TIME", 1440)

	return AuthConfig{
		AccessSecret:  getEnv("ACCESS_SECRET", ""),
		ExpAccTime:    expAccTime,
		RefreshSecret: getEnv("REFRESH_SECRET", ""),
		ExpRefTime:    expRefTime,
	}
}

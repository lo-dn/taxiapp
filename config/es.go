package config

type ElasticConfig struct {
	Protocol string
	Host     string
	Port     string
}

func LoadElasticConfig() ElasticConfig {
	return ElasticConfig{
		Protocol: getEnv("ELASTIC_PROTOCOL", ""),
		Host:     getEnv("ELASTIC_HOST", ""),
		Port:     getEnv("ELASTIC_PORT", ""),
	}
}

package config

type RedisConfig struct {
	UsingRedisPubSub bool
	Host             string
	Port             string
	Password         string
}

func LoadRedisConfig() RedisConfig {
	return RedisConfig{
		UsingRedisPubSub: getEnvAsBool("USING_REDIS_PUBS", false),
		Host:             getEnv("REDIS_HOST", ""),
		Port:             getEnv("REDIS_PORT", ""),
		Password:         getEnv("REDIS_PASSWORD", ""),
	}
}

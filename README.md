# taxiapp-lozovoi

## Task description

1) https://docs.google.com/document/d/1ZKH1TM33YLhIy2qyN5l5cfW3vziUFFeG2ae0f8PSE1c
2) https://docs.google.com/document/d/1crtgMu_w8n1PsiUoEoBtzE_dyUUMeBXDuyQ7HwplRxk

## Notes
* Redis Pub/Sub - is implemented for sending messages to multiple instances of the application.
* mockgen - https://github.com/golang/mock
* 


## How use

    make run - run project in docker container. 
    make run_taxi - run 'taxi' in IDE. 
    make run_simulator - run 'taxi simulator' in IDE.
    make link - control of the code style. 

    (For work with migration, please set db_link in makefile)
    make migrate-status - show the current state of migrations for the DB. 
    make migrate-up - rollout one migration. 
    make migrate-down - rollback one migration. 



-- +goose Up
-- +goose StatementBegin
create table users
(
    id int auto_increment,
    login varchar(30) not null,
    psw varchar(100) not null,
    created_at timestamp default now() null,
    last_online timestamp default now() null,
    constraint users_pk
        primary key (id)
)
comment 'Basic User Information';
-- +goose StatementEnd
-- +goose StatementBegin
create unique index users_login_uindex on users (login);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table users;
-- +goose StatementEnd

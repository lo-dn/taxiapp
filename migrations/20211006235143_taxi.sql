-- +goose Up
-- +goose StatementBegin
create table taxi
(
    id            int auto_increment,
    license_plate varchar(12) not null,
    brand         text null comment 'car brand type',
    body          text null comment 'car body type',
    constraint taxi_pk
        primary key (id)
);
-- +goose StatementEnd
-- +goose StatementBegin
create unique index taxi_license_plate_uindex
    on taxi (license_plate);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table taxi;
-- +goose StatementEnd

-- +goose Up
-- +goose StatementBegin
create table orders
(
    id int auto_increment,
    taxi_id int null,
    statuses_id int null default 1,
    created_at timestamp default now() null,
    constraint orders_pk
        primary key (id),
    constraint orders_taxi_id_fk
        foreign key (taxi_id) references taxi (id),
    constraint orders_statuses_id_fk
        foreign key (statuses_id) references statuses (id)
)
comment 'Information about trips';
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table orders;
-- +goose StatementEnd

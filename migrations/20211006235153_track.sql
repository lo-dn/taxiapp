-- +goose Up
-- +goose StatementBegin
create table track
(
    id int auto_increment,
    orders_id int null,
    geo_path TEXT null,
    created_at TIMESTAMP default now() null,
    constraint track_pk
        primary key (id)
)
comment 'Geo path of taxi movement during the order';

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table track;
-- +goose StatementEnd

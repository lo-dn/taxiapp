-- +goose Up
-- +goose StatementBegin
create table statuses
(
    id int auto_increment,
    name varchar(24) not null,
    description text null,
    constraint statuses_pk
        primary key (id)
)
comment 'Order status';
-- +goose StatementEnd
-- +goose StatementBegin
create unique index statuses_name_uindex on statuses (name);
-- +goose StatementEnd
-- +goose StatementBegin
INSERT INTO statuses(name, description) VALUES ('created', 'The order is ready to work');
-- +goose StatementEnd
-- +goose StatementBegin
INSERT INTO statuses(name, description) VALUES ('in_progress', 'The order is in progress');
-- +goose StatementEnd
-- +goose StatementBegin
INSERT INTO statuses(name, description) VALUES ('done', 'The order is completed');
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table statuses;
-- +goose StatementEnd

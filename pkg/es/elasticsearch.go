package es

import (
	"context"
	"fmt"
	"io/ioutil"
	"taxiapp/config"

	"github.com/olivere/elastic/v7"
)

type ElasticSearch struct {
	Client *elastic.Client
}

func NewElasticSearch(conf *config.ElasticConfig) (*ElasticSearch, error) {
	// eUsername := "username"
	// ePassword := "password"
	options := []elastic.ClientOptionFunc{
		elastic.SetSniff(false),
		elastic.SetURL(conf.Protocol + "://" + conf.Host + ":" + conf.Port),
		// elastic.SetBasicAuth(eUsername, ePassword),
	}

	client, err := elastic.NewClient(options...)
	if err != nil {
		return nil, fmt.Errorf("elastic.NewClient: %w", err)
	}

	return &ElasticSearch{
		Client: client,
	}, nil
}

func (e *ElasticSearch) CreateIndex(index, templatePath string) error {
	alias := index + "_alias"
	template := index + "_template"

	ctx := context.Background()
	body, _ := ioutil.ReadFile(templatePath)

	_, err := e.Client.IndexPutTemplate(template).BodyString(string(body)).Do(ctx)
	if err != nil {
		return fmt.Errorf("cannot create Index template: %w", err)
	}

	exists, err := e.Client.IndexExists(index).Do(ctx)
	if err != nil {
		return fmt.Errorf("cannot check Index existence: %w", err)
	}
	if exists {
		return nil
	}

	_, err = e.Client.CreateIndex(index).BodyString("").Do(ctx)
	if err != nil {
		return fmt.Errorf("cannot create Index: %w", err)
	}

	_, err = e.Client.Alias().Add(index, alias).Do(ctx)
	if err != nil {
		return fmt.Errorf("cannot create Index alias: %w", err)
	}

	return nil
}

package builder

import (
	"encoding/json"
	"fmt"

	"github.com/olivere/elastic/v7"
)

type QueryBuilder struct {
	q *elastic.BoolQuery
}

func NewQueryBuilder() *QueryBuilder {
	return &QueryBuilder{
		q: elastic.NewBoolQuery(),
	}
}

func (b QueryBuilder) Query() elastic.Query {
	return b.q
}

func (b *QueryBuilder) Search(query string) *QueryBuilder {
	b.q = b.q.Should(
		elastic.NewQueryStringQuery(query).Boost(2).DefaultOperator("AND").TieBreaker(0.4),
		elastic.NewQueryStringQuery(query).Boost(1).DefaultOperator("OR").TieBreaker(0.1),
	)

	return b
}

func (b *QueryBuilder) TermQuery(termKey, termValue string) *QueryBuilder {
	b.q = b.q.Must(elastic.NewTermQuery(termKey, termValue))

	return b
}

func (b *QueryBuilder) BoolKeywordQuery(termKey, termValue string) *QueryBuilder {
	return b.TermQuery(termKey+".keyword", termValue)
}

func (b *QueryBuilder) TermsQuery(termKey string, vals []interface{}) *QueryBuilder {
	if len(vals) == 0 {
		return b
	}

	b.q = b.q.Must(elastic.NewTermsQuery(termKey, vals...))

	return b
}

func (b *QueryBuilder) RangeQuery(termKey string, min, max int) *QueryBuilder {
	if min == 0 && max == 0 {
		return b
	}

	rangeQuery := elastic.NewRangeQuery(termKey)

	if min != 0 {
		rangeQuery = rangeQuery.Gte(min)
	}

	if max != 0 {
		rangeQuery = rangeQuery.Lte(max)
	}

	b.q = b.q.Must(rangeQuery)

	return b
}

func (b *QueryBuilder) DebugPrint() *QueryBuilder {
	fmt.Printf("=================\n=  DEBUG START  =\n=================\n")

	source, _ := b.Query().Source()
	sourceJson, _ := json.Marshal(source)
	fmt.Printf("Query:\n%s\n", string(sourceJson))

	fmt.Printf("=================\n=   DEBUG END   =\n=================\n")

	return b
}

package db

import (
	"errors"
	"taxiapp/config"
)

func Init(cnf config.DBConfig) (*MySql, error) {
	link, err := SetConfigLink(cnf.Host, cnf.Port, cnf.Name, cnf.User, cnf.Password)
	if err != nil {
		return nil, err
	}

	msqlIns := MySql{}
	if err := msqlIns.connect(link); err != nil {
		return nil, err
	}

	return &msqlIns, nil
}

func SetConfigLink(host, port, baseName, user, pass string) (string, error) {
	err := checkConfig(host, port, baseName, user, pass)
	if err != nil {
		return "", err
	}

	return user + ":" + pass + "@tcp(" + host + ":" + port + ")/" + baseName, nil
}

func checkConfig(host, port, baseName, user, pass string) error {
	if host == "" {
		return errors.New("Cannot initialize connection to MySql, host not set. ")
	}

	if port == "" {
		return errors.New("Cannot initialize connection to MySql, port not set. ")
	}

	if baseName == "" {
		return errors.New("Cannot initialize connection to MySql, base-name not set. ")
	}

	if user == "" {
		return errors.New("Cannot initialize connection to MySql, user not set. ")
	}

	if pass == "" {
		return errors.New("Cannot initialize connection to MySql, pass not set. ")
	}

	return nil
}

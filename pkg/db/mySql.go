package db

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type MySql struct {
	DB *sqlx.DB
}

func (m *MySql) connect(configLink string) (err error) {
	m.DB, err = sqlx.Connect("mysql", configLink)
	if err != nil {
		return err
	}

	return m.DB.Ping()
}

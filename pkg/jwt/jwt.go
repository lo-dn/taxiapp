package jwt

import (
	"errors"
	"fmt"

	"github.com/golang-jwt/jwt"
)

// The current version of the token. Increment if the token structure changes
const versionToken = "1"

type AccessTokenData struct {
	Id int `json:"id"`
	jwt.StandardClaims
}

type RefreshTokenData struct {
	UserLogin string `json:"userLogin"`
	jwt.StandardClaims
}

func NewToken(tokenData jwt.Claims, key string) (string, error) {
	// CreateUser the token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, tokenData)

	// Sign and get the complete encoded token as a string
	tokenString, err := token.SignedString([]byte(key + versionToken))
	return tokenString, err
}

func NewAccessToken(tokenData *AccessTokenData, key string) (string, int64, error) {
	token, err := NewToken(tokenData, key)
	if err != nil {
		return "", 0, err
	}

	return token, tokenData.ExpiresAt, err
}

func NewRefreshToken(tokenData *RefreshTokenData, key string) (string, int64, error) {
	token, err := NewToken(tokenData, key)
	if err != nil {
		return "", 0, err
	}

	return token, tokenData.ExpiresAt, err
}

func IsValid(token, key string) bool {
	tn, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v ", token.Header["alg"])
		}

		return []byte(key + versionToken), nil
	})

	if err == nil && tn.Valid {
		return true
	}
	return false
}

func GetAccessTokenData(token, key string) (*AccessTokenData, error) {
	tk, err := jwt.ParseWithClaims(token, &AccessTokenData{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(key + versionToken), nil
	})
	if err != nil {
		return nil, err
	}
	if claims, ok := tk.Claims.(*AccessTokenData); ok && tk.Valid {
		return claims, nil
	}

	return nil, errors.New("No luck  ")
}

func GetRefreshTokenData(token, key string) (*RefreshTokenData, error) {
	tk, err := jwt.ParseWithClaims(token, &RefreshTokenData{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(key + versionToken), nil
	})
	if err != nil {
		return nil, err
	}
	if claims, ok := tk.Claims.(*RefreshTokenData); ok && tk.Valid {
		return claims, nil
	}

	return nil, errors.New("No luck  ")
}

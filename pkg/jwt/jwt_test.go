package jwt

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"github.com/golang-jwt/jwt"
)

var testAccessData = AccessTokenData{
	Id: 1,
	StandardClaims: jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Minute * 120).Unix(),
	},
}

var testRefreshData = RefreshTokenData{
	UserLogin: "Test-Login$% --1'",
	StandardClaims: jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Minute * 120).Unix(),
	},
}

var key = "testKey"

func TestAccessToken(t *testing.T) {
	token, _, err := NewAccessToken(&testAccessData, key)
	assert.NoError(t, err)

	if !IsValid(token, key) {
		t.Error("Token not valid")
	}

	data, err := GetAccessTokenData(token, key)
	assert.NoError(t, err)

	assert.Equal(t, data, &testAccessData)
}

func TestRefreshToken(t *testing.T) {
	token, _, err := NewRefreshToken(&testRefreshData, key)
	assert.NoError(t, err)

	if !IsValid(token, key) {
		t.Error("Token not valid")
	}

	data, err := GetRefreshTokenData(token, key)
	assert.NoError(t, err)

	assert.Equal(t, data, &testRefreshData)
}

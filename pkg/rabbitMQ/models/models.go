package models

import "github.com/streadway/amqp"

type IConsumer interface {
	Setup() error
	Listen(queueName, routingKey string, handlerFunc func(amqp.Delivery), description string, isClose *chan struct{}) error
}

type IEmitter interface {
	Setup() error
	Push(msg []byte, key string) error
}

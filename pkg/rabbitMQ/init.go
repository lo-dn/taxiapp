package rabbitMQ

import (
	"fmt"
	"taxiapp/config"
	"taxiapp/pkg/rabbitMQ/event"
	"taxiapp/pkg/rabbitMQ/topic"
)

const (
	TypeEvent = event.Name
	TypePubs  = topic.Name
)

func Init(conf *config.AMPQConfig, exType string) (*Rabbit, error) {
	rabbit, err := New(conf.Host, conf.Port, conf.User, conf.Pass, exType)
	if err != nil {
		return nil, fmt.Errorf("New Rabbit instance err: %w ", err)
	}

	err = rabbit.connect()
	if err != nil {
		return nil, fmt.Errorf("rabbit.connect err: %w ", err)
	}

	return rabbit, nil
}

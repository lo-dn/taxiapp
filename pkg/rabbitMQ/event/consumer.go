package event

import (
	"taxiapp/pkg/rabbitMQ/models"

	"github.com/streadway/amqp"
)

// Consumer for receiving AMPQ events
type Consumer struct {
	conn *amqp.Connection
}

func NewConsumer(conn *amqp.Connection) (models.IConsumer, error) {
	consumer := Consumer{
		conn: conn,
	}
	err := consumer.Setup()
	if err != nil {
		return &Consumer{}, err
	}

	return &consumer, nil
}

func (consumer *Consumer) Setup() error {
	channel, err := consumer.conn.Channel()
	if err != nil {
		return err
	}
	return declareExchange(channel)
}

func (consumer *Consumer) Listen(queueName, key string, handlerFunc func(amqp.Delivery), description string, isClose *chan struct{}) error {
	ch, err := consumer.conn.Channel()
	if err != nil {
		return err
	}
	defer ch.Close()

	_, err = ch.QueueDeclare(
		queueName,
		false,
		false,
		false,
		false,
		map[string]interface{}{
			"Description": description,
		}, // arguments
	)

	if err != nil {
		// Queue parameters do not match.
		return err
	}

	if err = ch.QueueBind(
		queueName,
		queueName+getExchangeName(), // bindingKey
		getExchangeName(),           // sourceExchange
		false,                       // noWait
		nil,                         // arguments
	); err != nil {
		return err
	}

	err = ch.Qos(10, 0, false)
	if err != nil {
		return err
	}

	deliveries, err := ch.Consume(
		queueName,
		"tagConsume",
		false, // auto-ack or noack
		false, // exclusive
		false, // noLocal
		false, // noWait
		nil,   // arguments
	)
	if err != nil {
		return err
	}

	done := make(chan bool)

	go func() {
		for d := range deliveries {
			handlerFunc(d)

			_ = d.Ack(false)
		}

		*isClose <- struct{}{}
	}()
	<-done

	return nil
}

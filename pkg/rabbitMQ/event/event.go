package event

import (
	"github.com/streadway/amqp"
)

const Name = "event"

func getExchangeName() string {
	return Name
}

func declareExchange(ch *amqp.Channel) error {
	return ch.ExchangeDeclare(
		getExchangeName(), // name
		"direct",          // type
		false,             // durable
		false,             // auto-deleted
		false,             // internal
		false,             // no-wait
		nil,               // arguments
	)
}

package rabbitMQ

import (
	"errors"
	"fmt"
	"taxiapp/pkg/rabbitMQ/event"
	"taxiapp/pkg/rabbitMQ/models"
	"taxiapp/pkg/rabbitMQ/topic"

	"github.com/streadway/amqp"
)

type Rabbit struct {
	conn       *amqp.Connection
	emitter    models.IEmitter
	consumer   models.IConsumer
	connString string
	exType     string
}

func New(host, port, user, pass, exType string) (*Rabbit, error) {
	err := checkConfig(host, port, user, pass)
	if err != nil {
		return nil, err
	}

	return &Rabbit{
		exType:     exType,
		connString: "amqp://" + user + ":" + pass + "@" + host + ":" + port,
	}, nil
}

func (r *Rabbit) connect() error {
	var err error

	r.conn, err = amqp.Dial(r.connString)
	if err != nil {
		return errors.New("Failed to connect to Rabbit. URL: " + r.connString)
	}

	if err := r.init(); err != nil {
		return err
	}

	return nil
}

func (r *Rabbit) init() error {
	switch r.exType {
	case event.Name:
		emitter, err := event.NewEventEmitter(r.conn)
		if err != nil {
			return fmt.Errorf("event.NewEventEmitter err: %w ", err)
		}
		r.emitter = emitter

		consumer, err := event.NewConsumer(r.conn)
		if err != nil {
			return fmt.Errorf("event.NewConsumer err: %w ", err)
		}
		r.consumer = consumer
	case topic.Name:
		emitter, err := topic.NewEmitter(r.conn)
		if err != nil {
			return fmt.Errorf("topic.NewEmitter err: %w ", err)
		}
		r.emitter = emitter

		consumer, err := topic.NewConsumer(r.conn)
		if err != nil {
			return fmt.Errorf("topic.NewConsumer err: %w ", err)
		}
		r.consumer = consumer
	default:
		return errors.New("Unknown exchange type ")
	}

	return nil
}

func (r *Rabbit) IsConnExist() bool {
	return r.conn != nil
}

func (r *Rabbit) TryRestart() bool {
	_ = r.connect()
	return r.conn != nil
}

func (r *Rabbit) SendMsg(msg []byte, queueName string) error {
	return r.emitter.Push(msg, queueName)
}

func (r *Rabbit) StartListener(queueName, routingKey string, handlerFunc func(amqp.Delivery), description string, isClose *chan struct{}) (err error) {
	return r.consumer.Listen(queueName, routingKey, handlerFunc, description, isClose)
}

func (r *Rabbit) Close() {
	if r.conn != nil {
		_ = r.conn.Close()
	}
}

func checkConfig(host, port, user, pass string) error {
	if host == "" {
		return errors.New("Cannot initialize connection to Rabbit, host not set.  RABBIT_HOST")
	}

	if port == "" {
		return errors.New("Cannot initialize connection to Rabbit, port not set.  RABBIT_PORT")
	}

	if user == "" {
		return errors.New("Cannot initialize connection to Rabbit, user login not set.  RABBIT_USER")
	}

	if pass == "" {
		return errors.New("Cannot initialize connection to Rabbit, user password not set.  RABBIT_PASS")
	}
	return nil
}

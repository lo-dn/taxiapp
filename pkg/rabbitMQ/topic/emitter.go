package topic

import (
	"log"
	"taxiapp/pkg/rabbitMQ/models"

	"github.com/streadway/amqp"
)

// Emitter for publishing AMQP events
type Emitter struct {
	conn *amqp.Connection
}

func NewEmitter(conn *amqp.Connection) (models.IEmitter, error) {
	emitter := Emitter{
		conn: conn,
	}

	err := emitter.Setup()
	if err != nil {
		return &Emitter{}, err
	}

	return &emitter, nil
}

func (e *Emitter) Setup() error {
	channel, err := e.conn.Channel()
	if err != nil {
		panic(err)
	}

	defer channel.Close()
	return declareExchange(channel)
}

func (e *Emitter) Push(msg []byte, key string) error {
	channel, err := e.conn.Channel()
	if err != nil {
		return err
	}

	defer channel.Close()

	err = channel.Publish(
		getExchangeName(),
		key,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        msg,
		},
	)
	if err != nil {
		log.Printf("channel.Publish err: %s -> %s", err.Error(), msg)
		return err
	}

	return nil
}

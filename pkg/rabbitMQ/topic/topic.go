package topic

import (
	"github.com/streadway/amqp"
)

const Name = "pub/sub"

func getExchangeName() string {
	return Name
}

func declareExchange(ch *amqp.Channel) error {
	return ch.ExchangeDeclare(
		getExchangeName(), // name
		"topic",           // type
		false,             // durable
		false,             // auto-deleted
		false,             // internal
		false,             // no-wait
		nil,               // arguments
	)
}

package wsClient

import (
	"fmt"
	"net/url"

	"github.com/gorilla/websocket"
)

type Client struct {
	conn *websocket.Conn
	url  url.URL
}

func New(url url.URL) *Client {
	return &Client{
		url: url,
	}
}

func (c *Client) Connect() (err error) {
	c.conn, _, err = websocket.DefaultDialer.Dial(c.url.String(), nil)
	return
}

func (c *Client) Send(msg []byte) (response []byte, err error) {
	err = c.conn.WriteMessage(websocket.TextMessage, msg)
	if err != nil {
		return nil, fmt.Errorf("websocket WriteMessage err: %w", err)
	}

	// receive message
	_, response, err = c.conn.ReadMessage()
	if err != nil {
		return nil, fmt.Errorf("websocket ReadMessage err: %w", err)
	}

	return
}

func (c *Client) Close() {
	c.conn.Close()
}

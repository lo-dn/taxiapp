package main

import (
	"log"
	app "taxiapp"
	"taxiapp/config"
)

// @title Taxi App API
// @version 0.9
// @description API Server for Taxi Application

// @host localhost:7788
// @BasePath /

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name authorization

func main() {
	cfg := config.NewConfig(".env")

	err := app.Run(cfg)
	if err != nil {
		log.Println("app.Run error: ", err.Error())
	}
}

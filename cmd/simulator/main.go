package main

import (
	"context"
	"flag"
	"log"
	"taxiapp/config"
	"taxiapp/internal/models"
	"taxiapp/internal/repository"
	"taxiapp/internal/service"
	"taxiapp/internal/service/simulator"
	"taxiapp/pkg/db"
	"time"

	"github.com/joho/godotenv"
)

func main() {
	ctx := context.Background()

	simArgs := new(models.SimArgs)

	// Get console arguments
	simArgs.Delay = flag.Int("delay", 1, "Delay between messages (second)")
	simArgs.Workers = flag.Int("workers", 3, "Number of parallel simulated taxi drivers")
	simArgs.TrackSize = flag.Int("track-size", 10, "Number of tracks in the route")

	flag.Parse()

	simArgs.DelayDuration = time.Duration(*simArgs.Delay)

	// Load config
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}

	// Init DB connection
	dbConf := config.LoadDBConfig()
	conn, err := db.Init(dbConf)
	if err != nil {
		log.Fatalf("db.Init: %s ", err.Error())
	}

	repo, _ := repository.NewRepository(&ctx, conn.DB, nil)
	services := service.NewService(repo)

	sim := simulator.New(simArgs, services)

	if err := sim.PrepareData(); err != nil {
		log.Fatalf("PrepareData for simulator: %s ", err.Error())
	}

	if err := sim.Run(); err != nil {
		log.Fatalf("sim.Run: %s ", err.Error())
	}
}

package models

type OrderMySql struct {
	Id         int    `json:"id" db:"id"`
	TaxiId     int    `json:"taxiId" db:"taxi_id"`
	StatusesId int    `json:"statusesId" db:"statuses_id"`
	CreatedAt  string `json:"createdAt" db:"created_at" example:"2021-01-02 15:20:33"`
}

type OrderNormalize struct {
	Id           int    `json:"id" db:"id"`
	LicensePlate string `json:"licensePlate" db:"license_plate"`
	StatusName   string `json:"statusName" db:"name"`
	CreatedAt    string `json:"createdAt" db:"created_at"`
}

type TaxiMySql struct {
	Id           int    `json:"id" db:"id"`
	LicensePlate string `json:"licensePlate" db:"license_plate"`
	Brand        string `json:"brand"  db:"brand"`
	Body         string `json:"body"  db:"body"`
}

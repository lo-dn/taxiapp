package models

type TaxiEs struct {
	LicensePlate string `json:"licensePlate"`
	Brand        string `json:"brand"`
	Body         string `json:"body"`
	CreatedAt    string `json:"created_at"`
}

type TrackEs struct {
	Id          int         `json:"id"`
	OrderId     int         `json:"orderId"`
	Coordinates string      `json:"coordinates"`
	CreatedAt   string      `json:"createdAt"`
	Distance    interface{} `json:"distance"`
}

type TaxiFilter struct {
	Search string
	Filter map[string][]interface{}
	Sort   map[string]interface{}
	Limit  int
	Offset int
}

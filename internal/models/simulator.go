package models

import "time"

type SimArgs struct {
	Delay         *int
	DelayDuration time.Duration
	Workers       *int
	TrackSize     *int
}

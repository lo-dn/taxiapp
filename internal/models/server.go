package models

import (
	"taxiapp/config"
	"taxiapp/internal/delivery/amqp/subs"
	"taxiapp/internal/delivery/redis"
	"taxiapp/pkg/rabbitMQ"

	"github.com/jmoiron/sqlx"
)

type Server struct {
	DB    *sqlx.DB
	Conf  *config.Config
	AMPQ  *rabbitMQ.Rabbit
	Subs  *subs.Subs
	Redis *redis.Redis
}

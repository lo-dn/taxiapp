package models

type UserMySql struct {
	Id         int    `db:"id"`
	Login      string `db:"login"`
	Psw        string `db:"psw"`
	CreatedAt  string `db:"created_at"`
	LastOnline string `db:"last_online"`
}

type UserAuthInfo struct {
	Login         string `json:"login"`
	AccessToken   string `json:"accessToken"`
	RefreshToken  string `json:"refreshToken"`
	ExpAccTokenAt int64  `json:"expAccTokenAt"`
	ExpRefTokenAt int64  `json:"expRefTokenAt"`
}

package models

type TrackInfo struct {
	OrderId     int    `json:"orderId"`
	Coordinates string `json:"coordinates"`
}

type Track struct {
	Id          int    `json:"id"`
	Coordinates string `json:"coordinates" db:"geo_path"`
	CreatedAt   string `json:"createdAt" db:"created_at"`
}

package subs

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"taxiapp/config"
	"taxiapp/pkg/rabbitMQ"
	"time"

	"github.com/streadway/amqp"
)

var RoutingKey = "PubSub"

type msgData struct {
	Msg    []byte `json:"msg"`
	ChName string `json:"chName"`
}

type Subs struct {
	conf        *config.AMPQConfig
	conn        *rabbitMQ.Rabbit
	subName     string
	subscribers map[string]map[*chan []byte]chan []byte
}

func New(conf *config.AMPQConfig) (*Subs, error) {
	sub := Subs{
		conf:        conf,
		subName:     RoutingKey + "." + randName(8),
		subscribers: map[string]map[*chan []byte]chan []byte{},
	}

	err := sub.connect()
	if err != nil {
		return nil, err
	}

	go sub.listener()

	return &sub, nil
}

func (r *Subs) connect() error {
	conn, err := rabbitMQ.Init(r.conf, rabbitMQ.TypePubs)
	if err != nil {
		return fmt.Errorf("rabbitMQ.Init err: %w", err)
	}

	if !conn.IsConnExist() {
		return fmt.Errorf("No connection to rabbit exist. ")
	}

	r.conn = conn

	return nil
}

func (r *Subs) listener() {
	isClose := make(chan struct{})

	go func() {
		for range isClose {
			log.Println("Close sub listener")
			r.reconnect()
			return
		}
	}()

	err := r.conn.StartListener(r.subName, RoutingKey,
		func(delivery amqp.Delivery) {
			data := msgData{}

			if err := json.Unmarshal(delivery.Body, &data); err != nil {
				log.Println("listener Unmarshal err: ", err.Error())
				return
			}

			log.Println(r.subName+" get msg: ", data)
			for _, subscriber := range r.subscribers[data.ChName] {
				subscriber <- data.Msg
			}
		}, "Pub/Sub", &isClose,
	)
	if err != nil {
		log.Println("Don't start Pub/Sub Listener: ", err.Error())
		return
	}
}

func (r *Subs) Subscribe(ch string) *chan []byte {
	subscriber := make(chan []byte, 10)
	if r.subscribers[ch] == nil {
		r.subscribers[ch] = map[*chan []byte]chan []byte{}
	}
	r.subscribers[ch][&subscriber] = subscriber

	return &subscriber
}

func (r *Subs) Unsubscribe(ch string, subscriber *chan []byte) {
	delete(r.subscribers[ch], subscriber)
}

func (r *Subs) SendToChannel(msg []byte, ch string) error {
	data, err := json.Marshal(msgData{
		msg, ch,
	})
	if err != nil {
		return fmt.Errorf("json.Marshal(data) err: %w", err)
	}

	return r.conn.SendMsg(data, RoutingKey)
}

func (r *Subs) CloseChanel(ch string) {
	delete(r.subscribers, ch)
}

func (r *Subs) reconnect() {
	ticker := time.NewTicker(time.Minute)
	go func() {
		for range ticker.C {
			if err := r.connect(); err == nil {
				log.Println("Success restart connection")

				go r.listener()
				ticker.Stop()
				return
			}
		}
	}()
}

func randName(n int) string {
	rand.Seed(time.Now().UnixNano())

	letters := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	numbers := []rune("0123456789")

	s := make([]rune, n)
	for i := range s {
		if i < 2 || i > 5 {
			s[i] = letters[rand.Intn(len(letters))]
		} else {
			s[i] = numbers[rand.Intn(len(numbers))]
		}
	}
	return string(s)
}

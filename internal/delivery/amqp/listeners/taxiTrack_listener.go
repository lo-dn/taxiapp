package listeners

import (
	"encoding/json"
	"errors"
	"log"
	"taxiapp/internal/models"
	"taxiapp/internal/service"
	"taxiapp/internal/service/taxi"
	"taxiapp/pkg/rabbitMQ"
	"time"

	"github.com/streadway/amqp"
)

func InitTaxiTrack(services *service.Service, serv *models.Server) error {
	rabbit, err := rabbitMQ.Init(&serv.Conf.AMPQ, rabbitMQ.TypeEvent)
	if err != nil {
		return err
	}

	if !rabbit.IsConnExist() {
		return errors.New("No connection to rabbit exist  ")
	}

	go startListener(rabbit, services, serv)
	return nil
}

func startListener(r *rabbitMQ.Rabbit, services *service.Service, serv *models.Server) {
	isClose := make(chan struct{})

	go func() {
		for range isClose {
			log.Println("Close TaxiTrack listener")
			tryRestartConnection(services, serv)
			return
		}
	}()

	err := r.StartListener(serv.Conf.AMPQ.Queues.TaxiTrack, "",
		func(delivery amqp.Delivery) {
			trackInfo := models.TrackInfo{}

			if err := json.Unmarshal(delivery.Body, &trackInfo); err != nil {
				log.Println("taxiTrackListener Unmarshal err: ", err.Error())
				return
			}

			orderTrack, err := services.Track.Create(&trackInfo)
			if err != nil {
				log.Println("taxiTrackListener services.Track.Create err: ", err.Error())
				return
			}

			data, _ := json.Marshal(orderTrack)

			if serv.Conf.Redis.UsingRedisPubSub {
				err = serv.Redis.SendToChannel(string(data), taxi.GetOrderChannelName(trackInfo.OrderId))
			} else {
				err = serv.Subs.SendToChannel(data, taxi.GetOrderChannelName(trackInfo.OrderId))
			}

			if err != nil {
				log.Println("taxiTrackListener Subs.SendToChannel err: ", err.Error())
				return
			}
		}, "Transfer of taxi geo path", &isClose,
	)
	if err != nil {
		log.Println("Don't start TaxiTrack Listener: ", err.Error())
		return
	}
}

func tryRestartConnection(services *service.Service, serv *models.Server) {
	ticker := time.NewTicker(time.Minute)
	go func() {
		for range ticker.C {
			// Try restart connection
			if err := InitTaxiTrack(services, serv); err == nil {
				log.Println("Success restart connection")
				ticker.Stop()
				return
			}
		}
	}()
}

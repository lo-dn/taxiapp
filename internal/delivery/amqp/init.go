package amqp

import (
	"fmt"
	"taxiapp/internal/delivery/amqp/listeners"
	"taxiapp/internal/delivery/amqp/subs"
	"taxiapp/internal/models"
	"taxiapp/internal/service"
)

func Init(services *service.Service, serv *models.Server) error {
	err := listeners.InitTaxiTrack(services, serv)
	if err != nil {
		return fmt.Errorf("listeners.InitTaxiTrack err:  %w ", err)
	}

	fmt.Println("InitTaxiTrack listener initiates successfully")

	if !serv.Conf.Redis.UsingRedisPubSub {
		sub, err := subs.New(&serv.Conf.AMPQ)
		if err != nil {
			return fmt.Errorf("subs.New err:  %w ", err)
		}
		serv.Subs = sub
	}

	return nil
}

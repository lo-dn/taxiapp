package http

import (
	"taxiapp/internal/delivery/redis"
	"taxiapp/internal/delivery/ws"
	"taxiapp/internal/models"
	"taxiapp/pkg/rabbitMQ"

	"github.com/gin-gonic/gin"
)

func Start(serv *models.Server, c *gin.Engine) error {
	gin.SetMode(serv.Conf.HTTP.Mode)

	// Launching the WS client hub
	ws.RunHub()

	ampq, err := rabbitMQ.Init(&serv.Conf.AMPQ, rabbitMQ.TypeEvent)
	if err != nil {
		return err
	}

	serv.AMPQ = ampq

	if serv.Conf.Redis.UsingRedisPubSub {
		rds := redis.New(&serv.Conf.Redis)
		if err := rds.Connect(); err != nil {
			return err
		}
		serv.Redis = rds
	}

	// Run server
	err = c.Run(serv.Conf.HTTP.Host + ":" + serv.Conf.HTTP.Port)
	if err != nil {
		return err
	}

	return nil
}

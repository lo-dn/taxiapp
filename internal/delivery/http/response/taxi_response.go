package response

import (
	"taxiapp/internal/models"
)

type TaxiCollectionResponse struct {
	TaxiCollection   []models.TaxiMySql `json:"taxiList"`
	TaxiCollectionEs []models.TaxiEs    `json:"taxiListEs"`
	Quantity         int                `json:"quantity"`
}

type TaxiResponse struct {
	Taxi models.TaxiMySql `json:"taxi"`
}

type FilterResponse struct {
	Filters map[string]int `json:"filters"`
}

package response

import (
	"taxiapp/internal/models"
)

type OrdersListResponse struct {
	OrdersList []models.OrderNormalize `json:"orders"`
	Quantity   int                     `json:"quantity"`
}

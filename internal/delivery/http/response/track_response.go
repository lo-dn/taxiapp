package response

import "taxiapp/internal/models"

type TrackListResponse struct {
	TrackList []models.Track `json:"trackList"`
	Quantity  int            `json:"quantity"`
}

type TrackListEsResponse struct {
	TrackList []models.TrackEs `json:"trackList"`
	Quantity  int              `json:"quantity"`
}

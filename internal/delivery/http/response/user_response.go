package response

import (
	"taxiapp/internal/models"
)

type UserResponse struct {
	User *models.UserAuthInfo `json:"user"`
}

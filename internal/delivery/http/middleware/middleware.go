package middleware

import (
	"net/http"
	"taxiapp/config"
	"taxiapp/internal/delivery/http/response"
	"taxiapp/pkg/jwt"

	"github.com/gin-gonic/gin"
)

const authorizationHeader = "authorization"

type Middleware struct {
	conf *config.AuthConfig
}

func NewMiddleware(conf *config.AuthConfig) *Middleware {
	return &Middleware{conf: conf}
}

func (m *Middleware) CheckToken(c *gin.Context) {
	header := c.GetHeader(authorizationHeader)
	if header == "" {
		response.ErrorResponse(c, http.StatusUnauthorized, "empty auth header")
		c.Abort()
		return
	}

	if !jwt.IsValid(header, m.conf.AccessSecret) {
		response.ErrorResponse(c, http.StatusUnauthorized, "authorization does not contain a valid access key")
		c.Abort()
		return
	}
}

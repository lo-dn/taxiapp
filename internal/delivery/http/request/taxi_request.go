package request

import (
	"taxiapp/internal/models"
)

type TaxiRequest struct {
	Taxi models.TaxiMySql `json:"taxi"`
}

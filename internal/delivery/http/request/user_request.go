package request

import (
	validation "github.com/go-ozzo/ozzo-validation"
)

const (
	minPathLength = 8
)

type BasicAuth struct {
	Login    string `json:"login" binding:"required" example:"qwerty"`
	Password string `json:"password" binding:"required" example:"1eD4_5ks"`
}

func (ba BasicAuth) Validate() error {
	return validation.ValidateStruct(&ba,
		validation.Field(&ba.Login, validation.Length(1, 30)),
		validation.Field(&ba.Password, validation.Length(minPathLength, 30)),
	)
}

type LoginRequest struct {
	BasicAuth
}

type RegisterRequest struct {
	BasicAuth
}

type RefreshRequest struct {
	Token string `json:"token"`
}

func (rr RegisterRequest) Validate() error {
	return rr.BasicAuth.Validate()
}

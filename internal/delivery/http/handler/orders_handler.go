package handler

import (
	"log"
	"net/http"
	"strconv"
	"taxiapp/internal/delivery/http/response"
	"taxiapp/internal/service"

	"github.com/gin-gonic/gin"
)

type Orders interface {
	GetAll(c *gin.Context)
	GetById(c *gin.Context)
}

type OrdersHandler struct {
	services *service.Service
}

func NewOrdersHandler(services *service.Service) *OrdersHandler {
	return &OrdersHandler{services: services}
}

// GetAll
// @Summary Get orders list
// @Security ApiKeyAuth
// @Tags Orders
// @Description Get orders list
// @ID get-orders-list
// @Accept json
// @Produce json
// @Param limit query int false "limit" default(10)
// @Param offset query int false "offset" default(0)
// @Success 200 {object} response.OrdersListResponse
// @Failure 500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/orders/ [get]
func (h *OrdersHandler) GetAll(c *gin.Context) {
	params := c.Request.URL.Query()

	limit, offset, err := GetLimitOffsetParams(params)
	if err != nil {
		response.ErrorResponse(c, http.StatusInternalServerError, "limit or offset is not type int")
		return
	}

	orderList, quantity, err := h.services.Order.GetListNormalize(limit, offset)
	if err != nil {
		log.Println("h.services.Order.GetListNormalize err: ", err)
		response.ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(c, response.OrdersListResponse{
		OrdersList: orderList,
		Quantity:   quantity,
	})
}

// GetById
// @Summary Get order by ID
// @Security ApiKeyAuth
// @Tags Orders
// @Description Get order by id
// @ID get-order-by-id
// @Accept json
// @Produce json
// @Param id path int true "Order ID"
// @Success 200 {object} models.OrderMySql
// @Failure 500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/orders/:id [get]
func (h *OrdersHandler) GetById(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))

	order, err := h.services.Order.Get(id)
	if err != nil {
		log.Println("h.services.Order.Get err: ", err)
		response.ErrorResponse(c, http.StatusInternalServerError, err.Error())
	}

	response.SuccessResponse(c, order)
}

package handler

import (
	"bytes"
	"net/http/httptest"
	"taxiapp/config"
	"taxiapp/internal/models"
	"taxiapp/internal/service"
	mock_service "taxiapp/internal/service/mocks"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestHandler_Login(t *testing.T) {
	conf := config.AuthConfig{
		AccessSecret: "test1",
		ExpAccTime:   1,
	}

	// Init Test Table
	tests := []struct {
		name                 string
		inputBody            string
		inputUser            models.UserMySql
		mockBehavior         func(r *mock_service.MockAuth, user *models.UserMySql)
		expectedStatusCode   int
		expectedResponseBody string
	}{
		{
			name:      "Success",
			inputBody: `{"login": "username", "password": "qwerty123"}`,
			inputUser: models.UserMySql{
				Login: "username",
				Psw:   "qwerty123",
			},
			mockBehavior: func(r *mock_service.MockAuth, user *models.UserMySql) {
				userRes := &models.UserMySql{
					Id:    1,
					Login: "username",
					Psw:   "$2a$10$jxIPIFAsSqit3tEvZIBEQOOyKKfcK6/xAMuXZaTaccZe/oD5tc67m",
				}

				r.EXPECT().GetUserByLogin(user.Login).Return(userRes, nil)
				r.EXPECT().CreateTokens(userRes, &conf).Return(&models.UserAuthInfo{
					Login:         userRes.Login,
					AccessToken:   "AccessToken",
					RefreshToken:  "RefreshToken",
					ExpAccTokenAt: 1636410968,
					ExpRefTokenAt: 1636410968,
				}, nil)
			},
			expectedStatusCode:   200,
			expectedResponseBody: `{"user":{"login":"username","accessToken":"AccessToken","refreshToken":"RefreshToken","expAccTokenAt":1636410968,"expRefTokenAt":1636410968}}`,
		},
		{
			name:                 "Required fields are empty",
			inputBody:            `{"login": "username", "password": ""}`,
			inputUser:            models.UserMySql{},
			mockBehavior:         func(r *mock_service.MockAuth, user *models.UserMySql) {},
			expectedStatusCode:   400,
			expectedResponseBody: `{"code":400,"message":"Required fields are empty"}`,
		},
		{
			name:                 "Required fields are empty or not valid",
			inputBody:            `{"login": "username", "password": "1234"}`,
			inputUser:            models.UserMySql{},
			mockBehavior:         func(r *mock_service.MockAuth, user *models.UserMySql) {},
			expectedStatusCode:   400,
			expectedResponseBody: `{"code":400,"message":"Required fields are empty or not valid"}`,
		},
		{
			name:      "Invalid credentials",
			inputBody: `{"login": "username", "password": "12345678"}`,
			inputUser: models.UserMySql{
				Login: "username",
				Psw:   "qwerty123",
			},
			mockBehavior: func(r *mock_service.MockAuth, user *models.UserMySql) {
				r.EXPECT().GetUserByLogin(user.Login).Return(&models.UserMySql{
					Id:    1,
					Login: "username",
					Psw:   "$2a$10$jxIPIFAsSqit3tEvZIBEQOOyKKfcK6/xAMuXZaTaccZe/oD5tc67m",
				}, nil)
			},
			expectedStatusCode:   401,
			expectedResponseBody: `{"code":401,"message":"Invalid credentials"}`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			// Init Dependencies
			c := gomock.NewController(t)
			defer c.Finish()

			repo := mock_service.NewMockAuth(c)
			test.mockBehavior(repo, &test.inputUser)

			services := &service.Service{Auth: repo}
			handler := Handler{Auth: NewAuthHandler(services, &conf), Services: services}

			// Init Endpoint
			r := gin.New()
			r.POST("/login", handler.Login)

			// CreateUser Request
			w := httptest.NewRecorder()
			req := httptest.NewRequest("POST", "/login",
				bytes.NewBufferString(test.inputBody))
			req.Header.Set("Content-Type", "application/json")

			// Make Request
			r.ServeHTTP(w, req)

			// Assert
			assert.Equal(t, w.Code, test.expectedStatusCode)
			assert.Equal(t, w.Body.String(), test.expectedResponseBody)
		})
	}
}

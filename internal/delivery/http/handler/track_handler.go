package handler

import (
	"log"
	"net/http"
	"strconv"
	"taxiapp/internal/delivery/http/response"
	"taxiapp/internal/service"

	"github.com/gin-gonic/gin"
)

type Track interface {
	GetList(c *gin.Context)
	GetListEs(c *gin.Context)
}

type TrackHandler struct {
	services *service.Service
}

func NewTrackHandler(services *service.Service) *TrackHandler {
	return &TrackHandler{services: services}
}

// GetList
// @Summary Get Track List
// @Security ApiKeyAuth
// @Tags Tracks
// @Description Get Track list
// @ID get-track-list
// @Accept json
// @Produce json
// @Param id path int true "Order ID"
// @Param limit query int false "limit" default(10)
// @Param offset query int false "offset" default(0)
// @Success 200 {object} response.OrdersListResponse
// @Failure 500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/orders/:id/track [get]
func (h *TrackHandler) GetList(c *gin.Context) {
	params := c.Request.URL.Query()

	id, _ := strconv.Atoi(c.Param("id"))
	limit, offset, err := GetLimitOffsetParams(params)
	if err != nil {
		response.ErrorResponse(c, http.StatusInternalServerError, "limit or offset is not type int")
		return
	}

	trackList, quantity, err := h.services.Track.GetListByOrderId(id, limit, offset)
	if err != nil {
		log.Println("h.services.Order.GetListByOrderId err: ", err)
		response.ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(c, response.TrackListResponse{
		TrackList: trackList,
		Quantity:  quantity,
	})
}

// GetListEs
// @Summary Get Track-ES List
// @Security ApiKeyAuth
// @Tags Tracks
// @Description Get Track list from ElasticSearch
// @ID get-track-es-list
// @Accept json
// @Produce json
// @Param id path int true "Order ID"
// @Param limit query int false "limit" default(10)
// @Param offset query int false "offset" default(0)
// @Success 200 {object} response.TrackListEsResponse
// @Failure 500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/orders/:id/track-es [get]
func (h *TrackHandler) GetListEs(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))

	trackEsList, quantity, err := h.services.Track.GetListEs(id)
	if err != nil {
		log.Println("h.services.Track.GetListEs err: ", err)
		response.ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(c, response.TrackListEsResponse{
		TrackList: trackEsList,
		Quantity:  quantity,
	})
}

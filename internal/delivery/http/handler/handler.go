package handler

import (
	"fmt"
	"log"
	"net/url"
	"strconv"
	"taxiapp/config"
	"taxiapp/internal/service"
)

type Handler struct {
	Auth
	Orders
	Taxi
	Track
	Services *service.Service
}

func NewHandler(services *service.Service, conf *config.AuthConfig) *Handler {
	return &Handler{
		Auth:     NewAuthHandler(services, conf),
		Orders:   NewOrdersHandler(services),
		Taxi:     NewTaxiHandler(services),
		Track:    NewTrackHandler(services),
		Services: services,
	}
}

func GetLimitOffsetParams(params url.Values) (int, int, error) {
	// default values
	limit := 10
	offset := 0

	var errLimit error
	var errOffset error

	if params.Get("limit") != "" {
		limit, errLimit = strconv.Atoi(params.Get("limit"))
	}

	if params.Get("offset") != "" {
		offset, errOffset = strconv.Atoi(params.Get("offset"))
	}

	if errLimit != nil || errOffset != nil {
		log.Println("strconv.Atoi err: ", errLimit.Error(), errOffset.Error())
		return 0, 0, fmt.Errorf("strconv.Atoi err: %s / %s", errLimit.Error(), errOffset.Error())
	}

	return limit, offset, nil
}

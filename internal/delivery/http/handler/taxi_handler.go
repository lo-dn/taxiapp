package handler

import (
	"log"
	"net/http"
	"strconv"
	"strings"
	"taxiapp/internal/delivery/http/request"
	"taxiapp/internal/delivery/http/response"
	"taxiapp/internal/models"
	"taxiapp/internal/service"
	"taxiapp/pkg/db"

	"github.com/gin-gonic/gin"
)

type Taxi interface {
	GetAll(c *gin.Context)
	GetAllEs(c *gin.Context)
	SearchEs(c *gin.Context)
	GetById(c *gin.Context)
	GetKeywords(c *gin.Context)
	Create(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
}

type TaxiHandler struct {
	services *service.Service
}

func NewTaxiHandler(services *service.Service) *TaxiHandler {
	return &TaxiHandler{services: services}
}

// GetAll
// @Summary Get Taxi List
// @Security ApiKeyAuth
// @Tags Taxi
// @Description Get Taxi list
// @ID get-taxi-list
// @Accept json
// @Produce json
// @Param limit query int false "limit" default(10)
// @Param offset query int false "offset" default(0)
// @Success 200 {object} response.TaxiCollectionResponse
// @Failure 500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/taxi/ [get]
func (h *TaxiHandler) GetAll(c *gin.Context) {
	params := c.Request.URL.Query()

	limit, offset, err := GetLimitOffsetParams(params)
	if err != nil {
		response.ErrorResponse(c, http.StatusInternalServerError, "limit or offset is not type int")
		return
	}

	taxiList, quantity, err := h.services.Taxi.GetList(limit, offset)
	if err != nil {
		log.Println("TaxiHandler h.services.Taxi.GetList err: ", err)
		response.ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(c, response.TaxiCollectionResponse{
		TaxiCollection: taxiList,
		Quantity:       quantity,
	})
}

// GetAllEs
// @Summary Get TaxiEs List
// @Security ApiKeyAuth
// @Tags Taxi
// @Description Get Taxi list from ElasticSearch
// @ID get-taxi-es-list
// @Accept json
// @Produce json
// @Param limit query int false "limit" default(10)
// @Param offset query int false "offset" default(0)
// @Success 200 {object} response.TaxiCollectionResponse
// @Failure 500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/taxi-es/ [get]
func (h *TaxiHandler) GetAllEs(c *gin.Context) {
	params := c.Request.URL.Query()

	limit, offset, err := GetLimitOffsetParams(params)
	if err != nil {
		response.ErrorResponse(c, http.StatusInternalServerError, "limit or offset is not type int")
		return
	}

	taxiListEs, quantity, err := h.services.Taxi.GetListEs(&models.TaxiFilter{Limit: limit, Offset: offset})
	if err != nil {
		log.Println("TaxiHandler h.services.Taxi.GetListEs err: ", err)
		response.ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(c, response.TaxiCollectionResponse{
		TaxiCollectionEs: taxiListEs,
		Quantity:         quantity,
	})
}

// GetKeywords
// @Summary Get Keywords List
// @Security ApiKeyAuth
// @Tags Taxi
// @Description Get Keywords list from ElasticSearch
// @ID get-GetKeywords-es-list
// @Accept json
// @Produce json
// @Param name path string true "Name of the taxi field"
// @Success 200 {object} response.FilterResponse
// @Failure 500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/taxi-es/filters/:name [get]
func (h *TaxiHandler) GetKeywords(c *gin.Context) {
	filters, err := h.services.Taxi.GetKeywords(c.Param("name"))
	if err != nil {
		log.Println("TaxiHandler h.services.Taxi.GetKeywords err: ", err)
		response.ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(c, response.FilterResponse{
		Filters: filters,
	})
}

// SearchEs
// @Summary Search Taxi in ES
// @Security ApiKeyAuth
// @Tags Taxi
// @Description Search Taxi in ES
// @ID create-taxi
// @Accept json
// @Produce json
// @Param params body models.TaxiFilter true "Taxi Filter"
// @Success 200 {object} response.TaxiCollectionResponse
// @Failure 400,500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/taxi-es/ [POST]
func (h *TaxiHandler) SearchEs(c *gin.Context) {
	var taxiFilter models.TaxiFilter
	if err := c.ShouldBind(&taxiFilter); err != nil {
		response.ErrorResponse(c, http.StatusBadRequest, "Required fields are empty")
		return
	}

	taxiListEs, quantity, err := h.services.Taxi.GetListEs(&taxiFilter)
	if err != nil {
		log.Println("TaxiHandler h.services.Taxi.GetListEs err: ", err)
		response.ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(c, response.TaxiCollectionResponse{
		TaxiCollectionEs: taxiListEs,
		Quantity:         quantity,
	})
}

func (h *TaxiHandler) GetById(c *gin.Context) {
	response.ErrorResponse(c, 405, "method not implemented")
}

// Create
// @Summary Create Taxi
// @Security ApiKeyAuth
// @Tags Taxi
// @Description Create a taxi (to be added to the DB and ES)
// @ID create-taxi
// @Accept json
// @Produce json
// @Param params body request.TaxiRequest true "Taxi content"
// @Success 200 {object} response.TaxiResponse
// @Failure 500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/taxi/ [POST]
func (h *TaxiHandler) Create(c *gin.Context) {
	var taxiRequest request.TaxiRequest
	if err := c.ShouldBind(&taxiRequest); err != nil {
		response.ErrorResponse(c, http.StatusBadRequest, "Required fields are empty")
		return
	}

	if err := h.services.Taxi.Create(&taxiRequest.Taxi); err != nil {
		if strings.Contains(err.Error(), db.AlreadyExistsCode) {
			response.ErrorResponse(c, http.StatusBadRequest, "A taxi with this license plate already exists")
			return
		}

		log.Println("TaxiHandler h.services.Taxi.Create err: ", err)
		response.ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(c, response.TaxiResponse{Taxi: taxiRequest.Taxi})
}

// Update
// @Summary Update Taxi
// @Security ApiKeyAuth
// @Tags Taxi
// @Description Update a taxi (Update in DB and ES)
// @ID update-taxi
// @Accept json
// @Produce json
// @Param id path int true "Taxi ID"
// @Param params body request.TaxiRequest true "Updated Taxi content"
// @Success 200 {object} response.TaxiResponse
// @Failure 500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/taxi/:id [PUT]
func (h *TaxiHandler) Update(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	var taxiRequest request.TaxiRequest
	if err := c.ShouldBind(&taxiRequest); err != nil {
		response.ErrorResponse(c, http.StatusBadRequest, "Required fields are empty")
		return
	}

	taxiRequest.Taxi.Id = id
	if err := h.services.Taxi.Update(&taxiRequest.Taxi); err != nil {
		if strings.Contains(err.Error(), "Error 1062") {
			response.ErrorResponse(c, http.StatusBadRequest, "A taxi with this license plate already exists")
			return
		}

		log.Println("TaxiHandler h.services.Taxi.Update err: ", err)
		response.ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(c, response.TaxiResponse{Taxi: taxiRequest.Taxi})
}

// Delete
// @Summary Delete Taxi
// @Security ApiKeyAuth
// @Tags Taxi
// @Description Delete Taxi (Delete in DB and ES)
// @ID delete-taxi
// @Accept json
// @Produce json
// @Param id path int true "Taxi ID"
// @Success 200
// @Failure 500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/taxi/:id [DELETE]
func (h *TaxiHandler) Delete(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))

	if err := h.services.Taxi.Delete(id); err != nil {
		log.Println("TaxiHandler h.services.Taxi.Delete err: ", err)
		response.ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	response.SuccessResponse(c, nil)
}

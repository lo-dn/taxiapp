package handler

import (
	"log"
	"net/http"
	"taxiapp/config"
	"taxiapp/internal/delivery/http/request"
	"taxiapp/internal/delivery/http/response"
	"taxiapp/internal/models"
	"taxiapp/internal/service"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type Auth interface {
	Login(c *gin.Context)
	Register(c *gin.Context)
	RefreshToken(c *gin.Context)
}

type AuthHandler struct {
	services *service.Service
	conf     *config.AuthConfig
}

func NewAuthHandler(services *service.Service, conf *config.AuthConfig) *AuthHandler {
	return &AuthHandler{services, conf}
}

// Login
// @Summary Login
// @Tags Users
// @Description login
// @ID login
// @Accept json
// @Produce json
// @Param input body request.LoginRequest true "User info"
// @Success 200 {string} response.UserResponse
// @Failure 400,401 {object} response.Error
// @Failure 500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/users/login [post]
func (h *AuthHandler) Login(c *gin.Context) {
	loginRequest := request.LoginRequest{}

	if err := c.ShouldBind(&loginRequest); err != nil {
		response.ErrorResponse(c, http.StatusBadRequest, "Required fields are empty")
		return
	}

	if err := loginRequest.Validate(); err != nil {
		response.ErrorResponse(c, http.StatusBadRequest, "Required fields are empty or not valid")
		return
	}

	user, err := h.services.Auth.GetUserByLogin(loginRequest.Login)
	if err != nil {
		log.Println("repository.GetUserByLogin err: ", err.Error())
		response.ErrorResponse(c, http.StatusInternalServerError, "Server error: getUser")
		return
	}

	if user.Id == 0 || (bcrypt.CompareHashAndPassword([]byte(user.Psw), []byte(loginRequest.Password)) != nil) {
		response.ErrorResponse(c, http.StatusUnauthorized, "Invalid credentials")
		return
	}

	userAuthInfo, err := h.services.CreateTokens(user, h.conf)
	if err != nil {
		log.Println("Token generation error: ", err.Error())
		response.ErrorResponse(c, http.StatusInternalServerError, "Server error: Token generation error")
		return
	}

	response.SuccessResponse(c, response.UserResponse{
		User: userAuthInfo,
	})
}

// Register
// @Summary Register
// @Tags Users
// @Description create user
// @ID create-user
// @Accept json
// @Produce json
// @Param input body request.RegisterRequest true "User info"
// @Success 200 {object} response.UserResponse
// @Failure 400,422 {object} response.Error
// @Failure 500 {object} response.Error
// @Failure default {object} response.Error
// @Router /api/users [post]
func (h *AuthHandler) Register(c *gin.Context) {
	registerRequest := new(request.RegisterRequest)

	if err := c.ShouldBind(&registerRequest); err != nil {
		response.ErrorResponse(c, http.StatusUnprocessableEntity, "Required fields are empty")
		return
	}

	if err := registerRequest.Validate(); err != nil {
		response.ErrorResponse(c, http.StatusBadRequest, "Required fields are empty or not valid")
		return
	}

	user, err := h.services.Auth.GetUserByLogin(registerRequest.Login)
	if err != nil {
		log.Println("repository.GetUserByLogin err: ", err.Error())
		response.ErrorResponse(c, http.StatusInternalServerError, "Server error: getUser")
		return
	}

	if user.Id != 0 {
		response.ErrorResponse(c, http.StatusBadRequest, "User already exists")
		return
	}

	user = &models.UserMySql{
		Login: registerRequest.Login,
		Psw:   registerRequest.Password,
	}

	idUser, err := h.services.Auth.CreateUser(user)
	if err != nil {
		response.ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	user.Id = idUser

	userAuthInfo, err := h.services.CreateTokens(user, h.conf)
	if err != nil {
		log.Println("Token generation error: ", err.Error())
		response.ErrorResponse(c, http.StatusInternalServerError, "Server error: Token generation error")
		return
	}

	response.SuccessResponse(c, response.UserResponse{
		User: userAuthInfo,
	})
}

// RefreshToken
// @Summary Refresh access token
// @Description Perform refresh access token
// @ID user-refresh
// @Tags Users
// @Accept json
// @Produce json
// @Param params body request.RefreshRequest true "Refresh token"
// @Success 200 {object} response.UserResponse
// @Failure 401,422 {object} response.Error
// @Router /api/users/refresh-token [post]
func (h *AuthHandler) RefreshToken(c *gin.Context) {
	refreshRequest := new(request.RefreshRequest)
	if err := c.ShouldBind(refreshRequest); err != nil {
		response.ErrorResponse(c, http.StatusUnprocessableEntity, "Required fields are empty")
		return
	}

	data, err := h.services.Auth.GetRefreshTokenData(refreshRequest.Token, h.conf.RefreshSecret)
	if err != nil {
		log.Println("repository.GetRefreshTokenData err: ", err.Error())
		response.ErrorResponse(c, http.StatusInternalServerError, "Server error: GetRefreshTokenData")
		return
	}

	user, err := h.services.Auth.GetUserByLogin(data.UserLogin)
	if err != nil {
		log.Println("repository.GetUserByLogin err: ", err.Error())
		response.ErrorResponse(c, http.StatusInternalServerError, "Server error: getUser")
		return
	}

	if user.Id == 0 {
		response.ErrorResponse(c, http.StatusUnauthorized, "User not found")
		return
	}

	userAuthInfo, err := h.services.CreateTokens(user, h.conf)
	if err != nil {
		log.Println("Token generation error: ", err.Error())
		response.ErrorResponse(c, http.StatusInternalServerError, "Server error: Token generation error")
		return
	}

	response.SuccessResponse(c, response.UserResponse{
		User: userAuthInfo,
	})
}

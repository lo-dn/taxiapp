package routes

import (
	"taxiapp/internal/delivery/http/handler"
	"taxiapp/internal/delivery/http/middleware"
	"taxiapp/internal/delivery/http/response"
	"taxiapp/internal/delivery/ws"
	"taxiapp/internal/models"

	"github.com/gin-gonic/gin"

	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"

	_ "taxiapp/docs"
)

func InitRoutes(serv *models.Server, h *handler.Handler) *gin.Engine {
	r := gin.Default()

	m := middleware.NewMiddleware(&serv.Conf.Auth)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	r.GET("/ws", func(c *gin.Context) {
		ws.NewClient(c.Writer, c.Request, serv, h.Services)
	})

	groupPublicApi := r.Group("/api")
	{
		groupPublicApi.POST("/users", h.Auth.Register)
		groupPublicApi.POST("/users/login", h.Auth.Login)
		groupPublicApi.POST("/users/refresh-token", h.Auth.RefreshToken)
	}

	groupApi := r.Group("/api", m.CheckToken)
	{
		groupOrders := groupApi.Group("/orders")
		{
			groupOrders.GET("/", h.Orders.GetAll)
			groupOrders.GET("/:id", h.Orders.GetById)
			groupOrders.GET("/:id/track", h.Track.GetList)
			groupOrders.GET("/:id/track-es", h.Track.GetListEs)
		}

		groupTaxi := groupApi.Group("/taxi")
		{
			groupTaxi.POST("/", h.Taxi.Create)
			groupTaxi.GET("/", h.Taxi.GetAll)
			groupTaxi.GET("/:id", h.Taxi.GetById)
			groupTaxi.PUT("/:id", h.Taxi.Update)
			groupTaxi.DELETE("/:id", h.Taxi.Delete)
		}

		groupTaxiEs := groupApi.Group("/taxi-es")
		{
			groupTaxiEs.GET("/", h.Taxi.GetAllEs)
			groupTaxiEs.POST("/", h.Taxi.SearchEs)

			groupTaxiFilterEs := groupTaxiEs.Group("/filters")
			{
				groupTaxiFilterEs.GET("/:name", h.Taxi.GetKeywords)
			}
		}

	}

	r.NoRoute(func(c *gin.Context) {
		response.ErrorResponse(c, 404, "Not found route")
	})

	return r
}

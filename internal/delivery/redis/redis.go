package redis

import (
	"fmt"
	"log"
	"taxiapp/config"

	"github.com/go-redis/redis"
)

type Redis struct {
	client      *redis.Client
	sub         *redis.PubSub
	conf        *config.RedisConfig
	channels    map[string]struct{}
	subscribers map[string]map[*chan []byte]chan []byte
}

func New(conf *config.RedisConfig) *Redis {
	return &Redis{
		conf:        conf,
		channels:    map[string]struct{}{},
		subscribers: map[string]map[*chan []byte]chan []byte{},
	}
}

func (r *Redis) InitChannel(ch string) {
	if _, ok := r.channels[ch]; !ok {
		r.channels[ch] = struct{}{}
		r.startSubscriber(ch)
	}
}

func (r *Redis) Connect() error {
	r.client = redis.NewClient(&redis.Options{
		Addr:     r.conf.Host + ":" + r.conf.Port,
		Password: r.conf.Password,
		/*TLSConfig: &tls.Config{MinVersion: tls.VersionTLS12},*/
	})

	_, err := r.client.Ping().Result()
	if err != nil {
		return fmt.Errorf("Failed to connect to redis: %w ", err)
	}

	return nil
}

func (r *Redis) SendToChannel(msg, current string) error {
	return r.client.Publish(current, msg).Err()
}

func (r *Redis) Subscribe(ch string) *chan []byte {
	r.InitChannel(ch)

	subscriber := make(chan []byte, 10)
	if r.subscribers[ch] == nil {
		r.subscribers[ch] = map[*chan []byte]chan []byte{}
	}
	r.subscribers[ch][&subscriber] = subscriber

	return &subscriber
}

func (r *Redis) Unsubscribe(ch string, subscriber *chan []byte) {
	delete(r.subscribers[ch], subscriber)
}

func (r *Redis) Close(ch string) {
	err := r.sub.Unsubscribe(ch)
	if err != nil {
		log.Println("failed to unsubscribe redis channel subscription:", err)
	}

	delete(r.channels, ch)
	delete(r.subscribers, ch)
}

func (r *Redis) startSubscriber(ch string) {
	go func() {
		r.sub = r.client.Subscribe(ch)
		messages := r.sub.Channel()
		// subscribers := r.subscribers[ch]

		for message := range messages {
			// send to all websocket sessions/peers
			for _, subscriber := range r.subscribers[ch] {
				subscriber <- []byte(message.Payload)
			}
		}
	}()
}

package request

type Data struct {
	FuncName string `json:"type"`
	Data     string `json:"data"`
}

type InitTrackData struct {
	TaxiId  int `json:"taxiId"`
	OrderId int `json:"orderId"`
}

type InitSubscribeData struct {
	OrderId int    `json:"orderId"`
	Jwt     string `json:"jwt"`
}

package taxiHandlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"taxiapp/internal/delivery/ws/clients"
	"taxiapp/internal/delivery/ws/middleware"
	"taxiapp/internal/delivery/ws/request"
	"taxiapp/internal/models"
	"taxiapp/internal/service/taxi"
	"taxiapp/pkg/wsClient"
)

type TrackHandlers struct {
	serv       *models.Server
	subsClient map[wsClient.Client]int
}

func NewTrackHandlers(serv *models.Server) *TrackHandlers {
	return &TrackHandlers{serv, make(map[wsClient.Client]int)}
}

func (t *TrackHandlers) InitTrack(c *clients.Client, data []byte) error {
	trackData := request.InitTrackData{}
	if err := json.Unmarshal(data, &trackData); err != nil {
		return err
	}

	c.Info.TaxiId = trackData.TaxiId
	c.Info.OrderId = trackData.OrderId

	if c.Serv.Conf.Redis.UsingRedisPubSub {
		t.serv.Redis.InitChannel(taxi.GetOrderChannelName(trackData.OrderId))
	}
	return c.Services.Order.UpdateStatus(c.Info.OrderId, 2)
}

func (t *TrackHandlers) SendTrack(msg string) error {
	if !t.serv.AMPQ.IsConnExist() {
		if !t.serv.AMPQ.TryRestart() {
			return errors.New("There is no connection to Rabbit. ")
		}
	}

	err := t.serv.AMPQ.SendMsg([]byte(msg), t.serv.Conf.AMPQ.Queues.TaxiTrack)
	if err != nil {
		return fmt.Errorf("SendTrack SendMsg: %w ", err)
	}

	return nil
}

func (t *TrackHandlers) EndTrack(c *clients.Client) error {
	if c.Serv.Conf.Redis.UsingRedisPubSub {
		t.serv.Redis.Close(taxi.GetOrderChannelName(c.Info.OrderId))
	} else {
		t.serv.Subs.CloseChanel(taxi.GetOrderChannelName(c.Info.OrderId))
	}

	err := c.Services.Order.UpdateStatus(c.Info.OrderId, 3)
	if err != nil {
		return err
	}

	// Get all tracks
	tracks, _, err := c.Services.Track.GetListByOrderId(c.Info.OrderId, 0, 0)
	if err != nil {
		return err
	}

	// Send tracks to ES
	for _, track := range tracks {
		trackEs := models.TrackEs{
			Id:          track.Id,
			OrderId:     c.Info.OrderId,
			Coordinates: track.Coordinates,
			CreatedAt:   track.CreatedAt,
		}

		if err := c.Services.Track.AddToEs(&trackEs); err != nil {
			return err
		}
	}

	c.Info = clients.ClientInfo{}
	return nil
}

func (t *TrackHandlers) SubscribeOnTrack(c *clients.Client, data []byte) error {
	trackData := request.InitSubscribeData{}
	if err := json.Unmarshal(data, &trackData); err != nil {
		return err
	}

	if !middleware.IsAuthClient(trackData.Jwt, c.Serv.Conf.Auth.AccessSecret) {
		return fmt.Errorf("jwt does not contain a valid access key ")
	}

	chanName := taxi.GetOrderChannelName(trackData.OrderId)
	if c.Serv.Conf.Redis.UsingRedisPubSub {
		c.Info.SubChan = t.serv.Redis.Subscribe(chanName)
	} else {
		c.Info.SubChan = t.serv.Subs.Subscribe(chanName)
	}
	go t.subListener(c, chanName, c.Serv.Conf.Redis.UsingRedisPubSub)

	return nil
}

func (t *TrackHandlers) subListener(c *clients.Client, ch string, UsingRedisPubSub bool) {
	for {
		select {
		case <-c.Context.Done():
			if UsingRedisPubSub {
				t.serv.Redis.Unsubscribe(ch, c.Info.SubChan)
			} else {
				t.serv.Subs.Unsubscribe(ch, c.Info.SubChan)
			}
			return

		case message := <-*c.Info.SubChan:
			c.Send <- message
		}
	}
}

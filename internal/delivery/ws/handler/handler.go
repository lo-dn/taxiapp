package handler

import (
	"encoding/json"
	"log"
	"taxiapp/internal/delivery/ws/clients"
	"taxiapp/internal/delivery/ws/request"
	"taxiapp/internal/delivery/ws/response"
	"taxiapp/internal/delivery/ws/status"
)

func RequestHandler(c *clients.Client, msg []byte) {
	var reqData request.Data
	var resData response.Data

	err := json.Unmarshal(msg, &reqData)
	if err != nil {
		resData.Message = status.IncorrectJsonRequest
		resData.Data = response.NewDataError(err)
	} else {
		resData = handleSwitch(c, reqData)
	}

	byteResponse, err := json.Marshal(resData)
	if err != nil {
		log.Println("ws requestHandler -> Error converting to json for response. ", err.Error())
		return
	}

	// send a response to the request
	c.Send <- byteResponse
}

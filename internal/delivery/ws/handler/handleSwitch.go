package handler

import (
	"fmt"
	"taxiapp/internal/delivery/ws/clients"
	"taxiapp/internal/delivery/ws/handler/taxiHandlers"
	"taxiapp/internal/delivery/ws/request"
	"taxiapp/internal/delivery/ws/response"
	"taxiapp/internal/delivery/ws/status"
)

func handleSwitch(c *clients.Client, data request.Data) (resp response.Data) {
	resp.Message = status.Success

	switch data.FuncName {
	case "initTrack": // The client informs about the order for which the route will be tracked
		taxiHandle := taxiHandlers.NewTrackHandlers(c.Serv)

		err := taxiHandle.InitTrack(c, []byte(data.Data))
		if err != nil {
			resp.Message = status.ErrorOther
			resp.Data = response.NewDataError(err)
			return
		}
	case "sendTrack": // The client sends the track
		taxiHandle := taxiHandlers.NewTrackHandlers(c.Serv)

		err := taxiHandle.SendTrack(data.Data)
		if err != nil {
			resp.Message = status.ErrorOther
			resp.Data = response.NewDataError(err)
			return
		}
	case "endTrack": // The client informs about the termination of the route
		taxiHandle := taxiHandlers.NewTrackHandlers(c.Serv)

		err := taxiHandle.EndTrack(c)
		if err != nil {
			fmt.Printf("EndTrack err: %s", err.Error())

			resp.Message = status.ErrorOther
			resp.Data = response.NewDataError(err)
			return
		}
	case "subscribeOnTrack": // The client subscribes to the route update
		taxiHandle := taxiHandlers.NewTrackHandlers(c.Serv)

		err := taxiHandle.SubscribeOnTrack(c, []byte(data.Data))
		if err != nil {
			resp.Message = status.ErrorOther
			resp.Data = response.NewDataError(err)
			return
		}
	default:
		resp.Message = status.NoDataToAnswer
	}

	return
}

package ws

import (
	"net/http"
	"taxiapp/internal/delivery/ws/clients"
	"taxiapp/internal/delivery/ws/handler"
	"taxiapp/internal/models"
	"taxiapp/internal/service"
)

var hub = clients.NewHub()

func RunHub() {
	go hub.Run()
}

func NewClient(w http.ResponseWriter, r *http.Request, serv *models.Server, services *service.Service) {
	clients.NewClient(hub, w, r, serv, services, handler.RequestHandler)
}

package status

const (
	Success              = "Success"
	IncorrectJsonRequest = "Incorrect json in request: "
	IncorrectJsonData    = "Incorrect data json. "
	IncorrectToken       = "Incorrect token. "
	NoDataToAnswer       = "There is no data to answer. "
	UnauthorizedUser     = "Unauthorized user."
	ErrorParseData       = "Error parse data."
	ErrorDB              = "Error in database."
	ErrorDBConnection    = "Error database connection."
	ErrorDBEmptyResponse = "Error DB returned the void."
	ErrorDBQuery         = "Error getting query data from the database."
	ErrorOther           = "Other Error"
)

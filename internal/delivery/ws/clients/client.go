package clients

import (
	"context"
	"log"
	"net/http"
	"taxiapp/internal/models"
	"taxiapp/internal/service"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 1024
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type Client struct {
	Hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	Send chan []byte

	Serv     *models.Server
	Services *service.Service

	Info ClientInfo

	Context context.Context
	cancel  context.CancelFunc
}

type ClientInfo struct {
	TaxiId  int
	OrderId int
	SubChan *chan []byte
}

func NewClient(hub *Hub, w http.ResponseWriter, r *http.Request, serv *models.Server, services *service.Service, callBack func(*Client, []byte)) {
	// NOTE: Allow to cross-domain requests
	if serv.Conf.HTTP.Mode == "debug" {
		upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("NewClient -> Error connect. ", err)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	client := &Client{
		Hub:      hub,
		conn:     conn,
		Send:     make(chan []byte, 256),
		Serv:     serv,
		Services: services,
		Context:  ctx,
		cancel:   cancel,
	}
	client.Hub.register <- client

	go client.writePump()
	go client.readPump(callBack)
}

func (c *Client) readPump(callBack func(*Client, []byte)) {
	defer func() {
		c.Hub.unregister <- c
		c.cancel()
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	_ = c.conn.SetReadDeadline(time.Now().Add(pongWait))

	c.conn.SetPongHandler(func(string) error {
		_ = c.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Println("readPump IsUnexpectedCloseError: ", err)
			}
			break
		}

		callBack(c, message)
	}
}

// writePump pumps messages from the hub to the websocket connection.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()

	for {
		select {
		case message, ok := <-c.Send:
			if !ok {
				_ = c.write(websocket.CloseMessage, []byte{})
				return
			}

			if err := c.write(websocket.TextMessage, message); err != nil {
				return
			}
		case <-ticker.C:
			if err := c.write(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

// write writes a message with the given message type and payload.
func (c *Client) write(mt int, payload []byte) error {
	_ = c.conn.SetWriteDeadline(time.Now().Add(writeWait))
	return c.conn.WriteMessage(mt, payload)
}

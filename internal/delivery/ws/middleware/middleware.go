package middleware

import "taxiapp/pkg/jwt"

func IsAuthClient(token, key string) bool {
	return jwt.IsValid(token, key)
}

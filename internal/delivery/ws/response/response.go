package response

type Data struct {
	Message string                 `json:"msg"`
	Data    map[string]interface{} `json:"data"`
}

func NewDataError(err error) map[string]interface{} {
	return map[string]interface{}{
		"err": err.Error(),
	}
}

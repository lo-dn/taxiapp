package taxi

import (
	"errors"
	"fmt"
	"taxiapp/internal/models"
	"taxiapp/internal/repository"
	"time"
)

type Taxi interface {
	Create(*models.TaxiMySql) error
	GetList(limit, offset int) ([]models.TaxiMySql, int, error)
	GetListEs(*models.TaxiFilter) ([]models.TaxiEs, int, error)
	GetKeywords(key string) (map[string]int, error)
	Update(*models.TaxiMySql) error
	Delete(id int) error
}

type TaxiService struct {
	mySql repository.ITaxiMySql
	es    repository.ITaxiElastic
}

func NewTaxiService(mySql repository.ITaxiMySql, es repository.ITaxiElastic) *TaxiService {
	return &TaxiService{mySql: mySql, es: es}
}

func (s *TaxiService) Create(taxi *models.TaxiMySql) error {
	err := s.mySql.Create(taxi)
	if err != nil {
		return fmt.Errorf("mySql.Create err: %w", err)
	}

	return s.es.Create(&models.TaxiEs{
		LicensePlate: taxi.LicensePlate,
		Brand:        taxi.Brand,
		Body:         taxi.Body,
		CreatedAt:    time.Now().Format("2006-01-02 15:04:05"),
	})
}

func (s *TaxiService) GetList(limit, offset int) ([]models.TaxiMySql, int, error) {
	taxiList, err := s.mySql.GetList(limit, offset)
	if err != nil {
		return nil, 0, fmt.Errorf("TaxiService s.mySql.GetList err: %w", err)
	}

	quantity, err := s.mySql.GetQuantity()
	if err != nil {
		return nil, 0, fmt.Errorf("TaxiService s.mySql.GetQuantity err: %w", err)
	}

	return taxiList, quantity, nil
}

func (s *TaxiService) GetListEs(filter *models.TaxiFilter) ([]models.TaxiEs, int, error) {
	return s.es.GetList(filter)
}

func (s *TaxiService) GetKeywords(key string) (map[string]int, error) {
	return s.es.GetKeywords(key)
}

func (s *TaxiService) Update(taxi *models.TaxiMySql) error {
	taxiOld, err := s.mySql.Get(taxi.Id)
	if err != nil {
		return err
	}

	if taxi.Id == 0 {
		return errors.New("The record being updated was not found. ")
	}

	if err := s.mySql.Update(taxi); err != nil {
		return fmt.Errorf("mySql.Update err: %w", err)
	}

	return s.es.UpdateByLicensePlace(taxiOld.LicensePlate, &models.TaxiEs{
		LicensePlate: taxi.LicensePlate,
		Brand:        taxi.Brand,
		Body:         taxi.Body,
	})
}

func (s *TaxiService) Delete(id int) error {
	taxi, err := s.mySql.Get(id)
	if err != nil {
		return err
	}

	if taxi.Id == 0 {
		return errors.New("The record being deleted was not found. ")
	}

	if err := s.mySql.Delete(id); err != nil {
		return fmt.Errorf("mySql.Delete err: %w", err)
	}

	return s.es.Delete(taxi.LicensePlate)
}

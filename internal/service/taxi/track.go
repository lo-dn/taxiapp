package taxi

import (
	"fmt"
	"strconv"
	"strings"
	"taxiapp/internal/models"
	"taxiapp/internal/repository"

	"github.com/olivere/elastic/v7"
)

type Track interface {
	Create(*models.TrackInfo) (*models.Track, error)
	AddToEs(*models.TrackEs) error
	GetListEs(orderId int) (trackListEs []models.TrackEs, quantity int, err error)
	GetListByOrderId(id, limit, offset int) (trackList []models.Track, quantity int, err error)
	GetQuantity(orderId int) (int, error)
}

type TrackService struct {
	repo repository.ITrackMySql
	es   repository.ITrackElastic
}

func NewTrackService(repo repository.ITrackMySql, es repository.ITrackElastic) *TrackService {
	return &TrackService{repo: repo, es: es}
}

func (s *TrackService) Create(trackInfo *models.TrackInfo) (*models.Track, error) {
	id, err := s.repo.Create(trackInfo)
	if err != nil {
		return nil, fmt.Errorf("s.repo.Create err: %w", err)
	}

	return s.repo.GetById(id)
}

func (s *TrackService) AddToEs(track *models.TrackEs) error {
	return s.es.Create(track)
}

func (s *TrackService) GetListEs(orderId int) ([]models.TrackEs, int, error) {
	lastTrack, err := s.repo.GetLastByOrderId(orderId)
	if err != nil {
		return nil, 0, err
	}

	if lastTrack.Id == 0 {
		return nil, 0, fmt.Errorf("there are no tracks for the order: %d ", orderId)
	}

	coordinates := strings.Split(lastTrack.Coordinates, ",")
	if len(coordinates) != 2 {
		return nil, 0, fmt.Errorf("incorrect coordinate format: %s ", lastTrack.Coordinates)
	}

	lat, _ := strconv.ParseFloat(coordinates[0], 64)
	lon, _ := strconv.ParseFloat(coordinates[1], 64)
	endPoint := elastic.GeoPoint{Lat: lat, Lon: lon}

	trackEsList, quantity, err := s.es.GetList(orderId, endPoint)
	if err != nil {
		return nil, 0, err
	}

	return trackEsList, quantity, nil
}

func (s *TrackService) GetListByOrderId(id, limit, offset int) ([]models.Track, int, error) {
	trackList, err := s.repo.GetByOrderId(id, limit, offset)
	if err != nil {
		return nil, 0, fmt.Errorf("TrackService s.repo.GetByOrderId err: %w", err)
	}

	quantity, err := s.repo.GetQuantity(id)
	if err != nil {
		return nil, 0, fmt.Errorf("TrackService s.repo.GetQuantity err: %w", err)
	}

	return trackList, quantity, nil
}

func (s *TrackService) GetQuantity(orderId int) (int, error) {
	return s.repo.GetQuantity(orderId)
}

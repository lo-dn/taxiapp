package taxi

import (
	"taxiapp/internal/models"
	"taxiapp/internal/repository/mocks"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func mockAuthService(t *testing.T) (Auth, *mock_repository.MockIUserMySql) {
	t.Helper()

	mockCtl := gomock.NewController(t)
	defer mockCtl.Finish()

	userRepo := mock_repository.NewMockIUserMySql(mockCtl)

	userService := NewAuthService(userRepo)

	return userService, userRepo
}

func TestNewAuthService_CreateUser(t *testing.T) {
	userService, userRepo := mockAuthService(t)

	userRepo.EXPECT().Create(gomock.Any()).Return(1, nil)

	_, err := userService.CreateUser(&models.UserMySql{Login: "qwe", Psw: "qwe"})

	assert.NoError(t, err)
}

package taxi

import (
	"fmt"
	"strconv"
	"taxiapp/internal/models"
	"taxiapp/internal/repository"
)

type Order interface {
	Create(*models.OrderMySql) error
	Get(id int) (*models.OrderMySql, error)
	GetListNormalize(limit, offset int) ([]models.OrderNormalize, int, error)
	GetListByStatuses(statuses []string, limit, offset int) ([]models.OrderMySql, error)
	UpdateStatus(id, statusId int) error
}

type OrderService struct {
	repo repository.IOrderMySql
}

func NewOrderService(repo repository.IOrderMySql) *OrderService {
	return &OrderService{repo: repo}
}

func (s *OrderService) Create(order *models.OrderMySql) error {
	return s.repo.Create(order)
}

func (s *OrderService) Get(id int) (*models.OrderMySql, error) {
	order, err := s.repo.Get(id)
	if err != nil {
		return nil, fmt.Errorf("OrderService s.mySql.Get err: %w", err)
	}

	return order, nil
}

func (s *OrderService) GetListNormalize(limit, offset int) ([]models.OrderNormalize, int, error) {
	orderList, err := s.repo.GetListNormalize(limit, offset)
	if err != nil {
		return nil, 0, fmt.Errorf("OrderService s.mySql.GetListNormalize err: %w", err)
	}

	quantity, err := s.repo.GetQuantity()
	if err != nil {
		return nil, 0, fmt.Errorf("OrderService s.mySql.GetQuantity err: %w", err)
	}

	return orderList, quantity, nil
}

func (s *OrderService) GetListByStatuses(statuses []string, limit, offset int) ([]models.OrderMySql, error) {
	return s.repo.GetListByStatuses(statuses, limit, offset)
}

func (s *OrderService) UpdateStatus(id, statusId int) error {
	return s.repo.UpdateStatus(id, statusId)
}

func GetOrderChannelName(orderId int) string {
	return "Order-" + strconv.Itoa(orderId)
}

package taxi

import (
	"fmt"
	"taxiapp/config"
	"taxiapp/internal/models"
	"taxiapp/internal/repository"
	"taxiapp/pkg/jwt"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type Auth interface {
	CreateUser(*models.UserMySql) (int, error)
	GetUserByLogin(login string) (*models.UserMySql, error)
	CreateTokens(user *models.UserMySql, conf *config.AuthConfig) (*models.UserAuthInfo, error)
	GenerateAccessToken(*models.UserMySql, *config.AuthConfig) (string, int64, error)
	GenerateRefreshToken(*models.UserMySql, *config.AuthConfig) (string, int64, error)
	GetRefreshTokenData(token, key string) (*jwt.RefreshTokenData, error)
}

type AuthService struct {
	repo repository.IUserMySql
}

func NewAuthService(repo repository.IUserMySql) *AuthService {
	return &AuthService{repo: repo}
}

func (s *AuthService) CreateUser(user *models.UserMySql) (int, error) {
	enPass, err := bcrypt.GenerateFromPassword(
		[]byte(user.Psw),
		bcrypt.DefaultCost,
	)
	if err != nil {
		return 0, fmt.Errorf("bcrypt.GenerateFromPassword err: %w", err)
	}

	user.Psw = string(enPass)
	return s.repo.Create(user)
}

func (s *AuthService) GetUserByLogin(login string) (*models.UserMySql, error) {
	return s.repo.GetByLogin(login)
}

func (s *AuthService) CreateTokens(user *models.UserMySql, conf *config.AuthConfig) (*models.UserAuthInfo, error) {
	accessToken, expAccAt, err := s.GenerateAccessToken(user, conf)
	if err != nil {
		return nil, fmt.Errorf("GenerateAccessToken err %w", err)
	}

	refreshToken, expRefAt, err := s.GenerateRefreshToken(user, conf)
	if err != nil {
		return nil, fmt.Errorf("GenerateAccessToken err %w", err)
	}

	return &models.UserAuthInfo{
		Login:         user.Login,
		AccessToken:   accessToken,
		RefreshToken:  refreshToken,
		ExpAccTokenAt: expAccAt,
		ExpRefTokenAt: expRefAt,
	}, nil
}

func (s *AuthService) GenerateAccessToken(user *models.UserMySql, conf *config.AuthConfig) (string, int64, error) {
	exAt := time.Now().Add(time.Minute * time.Duration(conf.ExpAccTime)).Unix()

	var tokenData jwt.AccessTokenData
	tokenData.Id = user.Id
	tokenData.StandardClaims.ExpiresAt = exAt

	return jwt.NewAccessToken(&tokenData, conf.AccessSecret)
}

func (s *AuthService) GenerateRefreshToken(user *models.UserMySql, conf *config.AuthConfig) (string, int64, error) {
	exAt := time.Now().Add(time.Minute * time.Duration(conf.ExpRefTime)).Unix()

	var tokenData jwt.RefreshTokenData
	tokenData.UserLogin = user.Login
	tokenData.StandardClaims.ExpiresAt = exAt

	return jwt.NewRefreshToken(&tokenData, conf.RefreshSecret)
}

func (s *AuthService) GetRefreshTokenData(token, key string) (*jwt.RefreshTokenData, error) {
	return jwt.GetRefreshTokenData(token, key)
}

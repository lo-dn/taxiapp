package simulator

import (
	"fmt"
	"log"
	"math/rand"
	"net/url"
	"sync"
	"taxiapp/internal/delivery/ws/request"
	"taxiapp/internal/models"
	"taxiapp/internal/repository/orders"
	"taxiapp/internal/service"
	"taxiapp/pkg/wsClient"
	"time"
)

type Simulator struct {
	taxiList  []models.TaxiMySql
	orderList []models.OrderMySql
	args      *models.SimArgs
	services  *service.Service
}

func New(args *models.SimArgs, services *service.Service) *Simulator {
	return &Simulator{
		args:     args,
		services: services,
	}
}

func (s *Simulator) PrepareData() (err error) {
	// Prepared taxi list
	s.taxiList, _, err = s.services.Taxi.GetList(*s.args.Workers, 0)
	if err != nil {
		return fmt.Errorf("PrepareData s.services.Taxi.GetList err: %w", err)
	}

	for len(s.taxiList) < *s.args.Workers {
		taxi := models.TaxiMySql{LicensePlate: randLicensePlace(8)}

		if err := s.services.Taxi.Create(&taxi); err != nil {
			return fmt.Errorf("PrepareData s.services.Taxi.Create err: %w", err)
		}

		s.taxiList = append(s.taxiList, taxi)
	}

	// Prepared order list
	s.orderList, err = s.services.Order.GetListByStatuses([]string{orders.StatusCreate, orders.StatusInProgress}, len(s.taxiList), 0)
	if err != nil {
		return fmt.Errorf("PrepareData s.services.Order.GetListByStatuses err: %w", err)
	}

	return nil
}

func (s *Simulator) Run() error {
	var wg sync.WaitGroup

	for _, t := range s.taxiList {
		var order models.OrderMySql
		leftTrackCount := *s.args.TrackSize

		lns := len(s.orderList)
		if lns > 0 {
			// Warn: The taxi driver takes a random order. Previously, another taxi driver could carry out the order.
			order = s.orderList[lns-1]
			s.orderList = s.orderList[:lns-1]

			trackQuantity, err := s.services.Track.GetQuantity(order.Id)
			if err != nil {
				return fmt.Errorf("s.services.Track.GetQuantity err: %w", err)
			}

			leftTrackCount = *s.args.TrackSize - trackQuantity
		} else {
			order.TaxiId = t.Id

			err := s.services.Order.Create(&order)
			if err != nil {
				return fmt.Errorf("s.services.Order.Create err: %w", err)
			}
		}

		wg.Add(1)
		go s.tracking(&wg, t, order, leftTrackCount, s.args.DelayDuration)
	}

	wg.Wait()
	return nil
}

func (s *Simulator) tracking(wg *sync.WaitGroup, t models.TaxiMySql, order models.OrderMySql, count int, delay time.Duration) {
	defer wg.Done()

	fmt.Println("Tracking: taxi_id =", t.Id, " order.id =", order.Id, "countTracks =", count)

	client := wsClient.New(url.URL{
		Scheme: "ws",
		Host:   "localhost:7788",
		Path:   "/ws",
	})

	if err := client.Connect(); err != nil {
		log.Println("wsClient Connect: ", err)
		return
	}
	defer client.Close()

	track := NewTrack(request.InitTrackData{TaxiId: t.Id, OrderId: order.Id})

	initTrack, err := track.InitTrack()
	if err != nil {
		log.Println("tracking track.InitTrack: ", err)
		return
	}

	if _, err := client.Send(initTrack); err != nil {
		log.Println("wsClient SendToChannel: ", err)
		return
	}

	for i := 0; i < count; i++ {
		sendTrack, err := track.SendTrack()
		if err != nil {
			log.Println("tracking track.sendTrack: ", err)
			return
		}

		if _, err := client.Send(sendTrack); err != nil {
			log.Println("wsClient SendToChannel: ", err)
			return
		}

		time.Sleep(delay * time.Second)
	}

	endTrack, err := track.EndTrack()
	if err != nil {
		log.Println("tracking track.endTrack: ", err)
		return
	}

	if _, err := client.Send(endTrack); err != nil {
		log.Println("wsClient SendToChannel: ", err)
		return
	}
}

func randLicensePlace(n int) string {
	rand.Seed(time.Now().UnixNano())

	letters := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	numbers := []rune("0123456789")

	s := make([]rune, n)
	for i := range s {
		if i < 2 || i > 5 {
			s[i] = letters[rand.Intn(len(letters))]
		} else {
			s[i] = numbers[rand.Intn(len(numbers))]
		}
	}
	return string(s)
}

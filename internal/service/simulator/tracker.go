package simulator

import (
	"encoding/json"
	"fmt"
	"math"
	"math/rand"
	"taxiapp/internal/delivery/ws/request"
	"taxiapp/internal/models"
)

type Track struct {
	data request.InitTrackData
}

func NewTrack(data request.InitTrackData) Track {
	return Track{
		data: data,
	}
}

func (t Track) InitTrack() ([]byte, error) {
	data, err := json.Marshal(t.data)
	if err != nil {
		return nil, fmt.Errorf("json.Marshal data err: %w", err)
	}

	reqJson, err := json.Marshal(request.Data{FuncName: "initTrack", Data: string(data)})
	if err != nil {
		return nil, fmt.Errorf("json.Marshal request err: %w", err)
	}

	return reqJson, nil
}

func (t Track) SendTrack() ([]byte, error) {
	data, err := json.Marshal(t.randTrackPosition())
	if err != nil {
		return nil, fmt.Errorf("json.Marshal data err: %w", err)
	}

	reqJson, err := json.Marshal(request.Data{FuncName: "sendTrack", Data: string(data)})
	if err != nil {
		return nil, fmt.Errorf("json.Marshal request err: %w", err)
	}

	return reqJson, nil
}

func (t Track) EndTrack() ([]byte, error) {
	data, err := json.Marshal(t.data)
	if err != nil {
		return nil, fmt.Errorf("json.Marshal data err: %w", err)
	}

	reqJson, err := json.Marshal(request.Data{FuncName: "endTrack", Data: string(data)})
	if err != nil {
		return nil, fmt.Errorf("json.Marshal request err: %w", err)
	}

	return reqJson, nil
}

func (t Track) randTrackPosition() interface{} {
	return models.TrackInfo{Coordinates: randomGeo(), OrderId: t.data.OrderId}
}

func randomGeo() string {
	rd := 84.6

	u := rand.Float64()
	v := rand.Float64()

	w := rd * math.Sqrt(u)
	t := 2 * math.Pi * v
	x := w * math.Cos(t)
	y := w * math.Sin(t)

	return fmt.Sprint(y) + "," + fmt.Sprint(x)
}

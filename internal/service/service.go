package service

import (
	"taxiapp/internal/repository"
	"taxiapp/internal/service/taxi"
)

//go:generate mockgen -source=service.go -destination=mocks/mock.go

type Auth interface {
	taxi.Auth
}

type Order interface {
	taxi.Order
}

type Track interface {
	taxi.Track
}

type Taxi interface {
	taxi.Taxi
}

type Service struct {
	Auth
	Order
	Track
	Taxi
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		Auth:  taxi.NewAuthService(repos.IUserMySql),
		Order: taxi.NewOrderService(repos.IOrderMySql),
		Track: taxi.NewTrackService(repos.ITrackMySql, repos.ITrackElastic),
		Taxi:  taxi.NewTaxiService(repos.ITaxiMySql, repos.ITaxiElastic),
	}
}

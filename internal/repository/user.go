package repository

import (
	"fmt"
	"taxiapp/internal/models"

	"github.com/jmoiron/sqlx"
)

type UserMySQL struct {
	db *sqlx.DB
}

func NewUserMySQL(db *sqlx.DB) *UserMySQL {
	return &UserMySQL{db: db}
}

func (r *UserMySQL) Create(user *models.UserMySql) (int, error) {
	query := "INSERT INTO users (login, psw) VALUES (?, ?)"

	res, err := r.db.Exec(query, user.Login, user.Psw)
	if err != nil {
		return 0, fmt.Errorf("db.Exec err: %w ", err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("res.LastInsertId() err: %w ", err)
	}

	return int(id), nil
}

func (r *UserMySQL) GetByLogin(login string) (*models.UserMySql, error) {
	var user models.UserMySql
	query := "SELECT * FROM users WHERE login=?"

	rows, err := r.db.Queryx(query, login)
	if err != nil {
		return nil, fmt.Errorf("db.Queryx err: %w ", err)
	}

	for rows.Next() {
		if err := rows.StructScan(&user); err != nil {
			return nil, fmt.Errorf("rows.StructScan err: %w ", err)
		}
	}

	return &user, nil
}

package repository

import (
	"context"
	"fmt"
	"taxiapp/internal/models"
	"taxiapp/internal/repository/orders"
	"taxiapp/pkg/es"

	"github.com/olivere/elastic/v7"

	"github.com/jmoiron/sqlx"
)

//go:generate mockgen -source=repository.go -destination=mocks/mock.go

type IUserMySql interface {
	Create(*models.UserMySql) (int, error)
	GetByLogin(login string) (*models.UserMySql, error)
}

type IOrderMySql interface {
	Create(*models.OrderMySql) error
	Get(orderId int) (*models.OrderMySql, error)
	GetList(limit, offset int) ([]models.OrderMySql, error)
	GetListNormalize(limit, offset int) ([]models.OrderNormalize, error)
	GetListByStatuses(statuses []string, limit, offset int) ([]models.OrderMySql, error)
	GetQuantity() (int, error)
	UpdateStatus(orderId, statusId int) error
}

type ITrackMySql interface {
	Create(*models.TrackInfo) (int, error)
	GetById(id int) (*models.Track, error)
	GetByOrderId(orderId, limit, offset int) ([]models.Track, error)
	GetQuantity(orderId int) (int, error)
	GetLastByOrderId(id int) (*models.Track, error)
}

type ITaxiMySql interface {
	Create(*models.TaxiMySql) error
	Get(id int) (*models.TaxiMySql, error)
	GetList(limit, offset int) ([]models.TaxiMySql, error)
	GetQuantity() (int, error)
	Update(*models.TaxiMySql) error
	Delete(id int) error
}

type ITaxiElastic interface {
	Create(*models.TaxiEs) error
	GetList(filter *models.TaxiFilter) ([]models.TaxiEs, int, error)
	GetByLicensePlace(id string) (*models.TaxiEs, error)
	UpdateByLicensePlace(id string, taxi *models.TaxiEs) error
	Delete(id string) error
	GetKeywords(key string) (map[string]int, error)
}

type ITrackElastic interface {
	Create(*models.TrackEs) error
	GetList(orderId int, endPoint elastic.GeoPoint) ([]models.TrackEs, int, error)
}

type Repository struct {
	IUserMySql
	IOrderMySql
	ITrackMySql
	ITaxiMySql
	ITaxiElastic
	ITrackElastic
}

func NewRepository(ctx *context.Context, db *sqlx.DB, es *es.ElasticSearch) (*Repository, error) {
	taxiEs, err := NewTaxiElastic(ctx, es)
	if err != nil {
		return nil, fmt.Errorf("NewTaxiElastic err: %w", err)
	}

	trackEs, err := NewTrackElastic(ctx, es)
	if err != nil {
		return nil, fmt.Errorf("NewTrackElastic err: %w", err)
	}

	return &Repository{
		IUserMySql:    NewUserMySQL(db),
		IOrderMySql:   orders.NewOrderMySQL(db),
		ITrackMySql:   NewTrackMySQL(db),
		ITrackElastic: trackEs,
		ITaxiMySql:    NewTaxiMySQL(db),
		ITaxiElastic:  taxiEs,
	}, nil
}

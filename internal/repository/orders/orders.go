package orders

import (
	"fmt"
	"taxiapp/internal/models"

	"github.com/jmoiron/sqlx"
)

const (
	StatusCreate     = "created"
	StatusInProgress = "in_progress"
	StatusInDone     = "done"
)

type OrderMySQL struct {
	db *sqlx.DB
}

func NewOrderMySQL(db *sqlx.DB) *OrderMySQL {
	return &OrderMySQL{db: db}
}

func (r *OrderMySQL) Create(order *models.OrderMySql) error {
	query := "INSERT INTO orders (taxi_id) VALUES (?)"

	res, err := r.db.Exec(query, order.TaxiId)
	if err != nil {
		return fmt.Errorf("r.db.Exec err: %w ", err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return fmt.Errorf("res.LastInsertId() err: %w ", err)
	}

	order.Id = int(id)

	return nil
}

func (r *OrderMySQL) Get(orderId int) (*models.OrderMySql, error) {
	var order models.OrderMySql
	query := "SELECT * FROM orders WHERE id=?"

	rows, err := r.db.Queryx(query, orderId)
	if err != nil {
		return nil, fmt.Errorf("r.db.Queryx err: %w ", err)
	}

	for rows.Next() {
		if err := rows.StructScan(&order); err != nil {
			return nil, fmt.Errorf("rows.StructScan err: %w ", err)
		}
	}

	return &order, nil
}

func (r *OrderMySQL) GetList(limit, offset int) ([]models.OrderMySql, error) {
	q := "SELECT * FROM orders LIMIT ? OFFSET ?"

	rows, err := r.db.Queryx(q, limit, offset)
	if err != nil {
		return nil, fmt.Errorf("r.db.Queryx err: %w ", err)
	}

	var orderList []models.OrderMySql
	for rows.Next() {
		var order models.OrderMySql

		if err := rows.StructScan(&order); err != nil {
			return nil, fmt.Errorf("rows.StructScan err: %w ", err)
		}

		orderList = append(orderList, order)
	}

	return orderList, nil
}

func (r *OrderMySQL) GetListNormalize(limit, offset int) ([]models.OrderNormalize, error) {
	query := "SELECT o.id, t.license_plate, s.name, o.created_at FROM orders o " +
		"left join taxi t on t.id = o.taxi_id " +
		"left join statuses s on s.id = o.statuses_id  order by o.id DESC LIMIT ? OFFSET ? "

	rows, err := r.db.Queryx(query, limit, offset)
	if err != nil {
		return nil, fmt.Errorf("r.db.Queryx err: %w ", err)
	}

	var orderList []models.OrderNormalize
	for rows.Next() {
		var order models.OrderNormalize

		if err := rows.StructScan(&order); err != nil {
			return nil, fmt.Errorf("rows.StructScan err: %w ", err)
		}

		orderList = append(orderList, order)
	}

	return orderList, nil
}

func (r *OrderMySQL) GetListByStatuses(statuses []string, limit, offset int) ([]models.OrderMySql, error) {
	q := "SELECT id, taxi_id, statuses_id, created_at FROM orders WHERE statuses_id in " +
		"(select id from statuses where name in (?)) LIMIT ? OFFSET ?"

	query, args, err := sqlx.In(q, statuses, limit, offset)
	if err != nil {
		return nil, fmt.Errorf("sqlx.In err: %w ", err)
	}

	rows, err := r.db.Queryx(query, args...)
	if err != nil {
		return nil, fmt.Errorf("r.db.Queryx err: %w ", err)
	}

	var orderList []models.OrderMySql
	for rows.Next() {
		var order models.OrderMySql

		if err := rows.StructScan(&order); err != nil {
			return nil, fmt.Errorf("rows.StructScan err: %w ", err)
		}

		orderList = append(orderList, order)
	}

	return orderList, nil
}

func (r *OrderMySQL) GetQuantity() (int, error) {
	var count int

	err := r.db.QueryRow("SELECT count(*) FROM orders").Scan(&count)
	return count, err
}

func (r *OrderMySQL) UpdateStatus(orderId, statusId int) error {
	query := "UPDATE orders SET statuses_id = ? WHERE id = ?"

	_, err := r.db.Exec(query, statusId, orderId)
	if err != nil {
		return fmt.Errorf("r.db.Exec err:  %w ", err)
	}

	return nil
}

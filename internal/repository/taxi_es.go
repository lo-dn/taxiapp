package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"taxiapp/internal/models"
	"taxiapp/pkg/es"
	"taxiapp/pkg/es/builder"
	"time"

	"github.com/olivere/elastic/v7"
)

type TaxiElastic struct {
	index    string
	tempPath string
	elastic  *es.ElasticSearch
	ctx      *context.Context
	timeout  time.Duration
}

func NewTaxiElastic(ctx *context.Context, elastic *es.ElasticSearch) (*TaxiElastic, error) {
	var (
		index        = "taxi_app"
		templatePath = "./template/es/taxi.json"
	)

	if elastic != nil {
		if err := elastic.CreateIndex(index, templatePath); err != nil {
			return nil, fmt.Errorf("CreateIndex err: %w", err)
		}
	}

	return &TaxiElastic{
		index:    index,
		tempPath: templatePath,
		elastic:  elastic,
		ctx:      ctx,
		timeout:  time.Second * 10,
	}, nil
}

func (p TaxiElastic) Create(taxi *models.TaxiEs) error {
	ctx, cancel := context.WithTimeout(*p.ctx, p.timeout)
	defer cancel()

	_, err := p.elastic.Client.Index().
		Index(p.index).
		Id(taxi.LicensePlate).
		BodyJson(taxi).
		Do(ctx)
	if err != nil {
		return fmt.Errorf("insert: request: %w", err)
	}

	return nil
}

func (p TaxiElastic) UpdateByLicensePlace(id string, taxi *models.TaxiEs) error {
	ctx, cancel := context.WithTimeout(*p.ctx, p.timeout)
	defer cancel()

	if id != taxi.LicensePlate {
		if err := p.Delete(id); err != nil {
			return err
		}
		if err := p.Create(taxi); err != nil {
			return err
		}
		return nil
	}

	_, err := p.elastic.Client.Update().
		Index(p.index).
		Id(id).
		Script(elastic.NewScriptInline("ctx._source.body = params.body").Param("body", taxi.Body)).
		Script(elastic.NewScriptInline("ctx._source.brand = params.brand").Param("brand", taxi.Brand)).
		// Upsert(map[string]interface{}{"body": taxi.Body}).
		Do(ctx)
	if err != nil {
		return fmt.Errorf("Update request err: %w ", err)
	}

	return nil
}

func (p TaxiElastic) Delete(id string) error {
	ctx, cancel := context.WithTimeout(*p.ctx, p.timeout)
	defer cancel()

	_, err := p.elastic.Client.Delete().
		Index(p.index).
		Id(id).
		Do(ctx)
	if err != nil {
		return fmt.Errorf("Delete request err: %w ", err)
	}

	return nil
}

func (p TaxiElastic) GetByLicensePlace(id string) (*models.TaxiEs, error) {
	ctx, cancel := context.WithTimeout(*p.ctx, p.timeout)
	defer cancel()

	get1, err := p.elastic.Client.Get().
		Index(p.index).
		Id(id).
		Do(ctx)
	if err != nil {
		return nil, fmt.Errorf("Client.Get() err: %w", err)
	}

	var t models.TaxiEs
	if err := json.Unmarshal(get1.Source, &t); err != nil {
		return nil, fmt.Errorf("json.Unmarshal err: %w", err)
	}
	return &t, nil
}

func (p TaxiElastic) GetList(filter *models.TaxiFilter) ([]models.TaxiEs, int, error) {
	var taxiList []models.TaxiEs

	ctx, cancel := context.WithTimeout(*p.ctx, p.timeout)
	defer cancel()

	b := builder.NewQueryBuilder()

	if filter.Search != "" {
		b.Search("*" + filter.Search + "*")
	}

	for i, v := range filter.Filter {
		b.TermsQuery(i, v)
	}

	// b.DebugPrint()
	query := b.Query()

	s := p.elastic.Client.Search().
		Index(p.index).
		From(filter.Offset).Size(filter.Limit).
		Query(query)

	for i, v := range filter.Sort {
		s.Sort(i, v.(bool))
	}

	res, err := s.Do(ctx)
	if err != nil {
		return nil, 0, fmt.Errorf("Client.Search() err: %w", err)
	}

	for _, hit := range res.Hits.Hits {
		var t models.TaxiEs
		if err := json.Unmarshal(hit.Source, &t); err != nil {
			continue
		}

		taxiList = append(taxiList, t)
	}

	return taxiList, int(res.TotalHits()), nil
}

func (p TaxiElastic) GetKeywords(key string) (map[string]int, error) {
	aggregationName := "agg_" + key

	ctx, cancel := context.WithTimeout(*p.ctx, p.timeout)
	defer cancel()

	result, _ := p.elastic.Client.Search().
		Index(p.index).
		Query(elastic.NewBoolQuery()).
		Size(0).
		Aggregation(aggregationName, elastic.NewTermsAggregation().Field(key).Size(1000)).
		Pretty(true).
		Do(ctx)

	var ar elastic.AggregationBucketKeyItems
	if err := json.Unmarshal(result.Aggregations[aggregationName], &ar); err != nil {
		return nil, fmt.Errorf("Unmarshal failed: %w ", err)
	}

	res := make(map[string]int)
	for _, item := range ar.Buckets {
		res[item.Key.(string)] = int(item.DocCount)
	}

	return res, nil
}

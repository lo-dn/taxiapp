package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"taxiapp/internal/models"
	"taxiapp/pkg/es"
	"taxiapp/pkg/es/builder"
	"time"

	"github.com/olivere/elastic/v7"
)

type TrackElastic struct {
	index    string
	tempPath string
	elastic  *es.ElasticSearch
	ctx      *context.Context
	timeout  time.Duration
}

func NewTrackElastic(ctx *context.Context, elastic *es.ElasticSearch) (*TrackElastic, error) {
	var (
		index        = "track_app"
		templatePath = "./template/es/track.json"
	)

	if elastic != nil {
		if err := elastic.CreateIndex(index, templatePath); err != nil {
			return nil, fmt.Errorf("CreateIndex err: %w", err)
		}
	}

	return &TrackElastic{
		index:    index,
		tempPath: templatePath,
		elastic:  elastic,
		ctx:      ctx,
		timeout:  time.Second * 10,
	}, nil
}

func (p TrackElastic) Create(track *models.TrackEs) error {
	ctx, cancel := context.WithTimeout(*p.ctx, p.timeout)
	defer cancel()

	_, err := p.elastic.Client.Index().
		Index(p.index).
		Id(strconv.Itoa(track.Id)).
		BodyJson(track).
		Do(ctx)
	if err != nil {
		return fmt.Errorf("insert: request: %w", err)
	}

	return nil
}

func (p TrackElastic) GetList(orderId int, endPoint elastic.GeoPoint) ([]models.TrackEs, int, error) {
	var trackList []models.TrackEs

	ctx, cancel := context.WithTimeout(*p.ctx, p.timeout)
	defer cancel()

	b := builder.NewQueryBuilder().TermsQuery("orderId", []interface{}{orderId})

	distance := elastic.NewScriptField("distance",
		elastic.NewScript("doc['coordinates'].arcDistance(params.lat, params.lon) * 0.001").
			Param("lat", endPoint.Lat).
			Param("lon", endPoint.Lon),
	)

	src := elastic.NewSearchSource().
		Query(b.Query()).
		ScriptFields(distance).
		StoredFields("_source")

	res, err := p.elastic.Client.Search().
		Index(p.index).
		SearchSource(src).
		Do(ctx)
	if err != nil {
		return nil, 0, err
	}

	for _, hit := range res.Hits.Hits {
		var t models.TrackEs
		if err := json.Unmarshal(hit.Source, &t); err != nil {
			continue
		}

		for _, v := range hit.Fields {
			t.Distance = v
		}

		trackList = append(trackList, t)
	}

	return trackList, int(res.TotalHits()), nil
}

package repository

import (
	"fmt"
	"taxiapp/internal/models"

	"github.com/jmoiron/sqlx"
)

type TaxiMySQL struct {
	db *sqlx.DB
}

func NewTaxiMySQL(db *sqlx.DB) *TaxiMySQL {
	return &TaxiMySQL{db: db}
}

func (r *TaxiMySQL) Create(taxi *models.TaxiMySql) error {
	query := "INSERT INTO taxi (license_plate, brand, body) VALUES (?, ?, ?)"

	res, err := r.db.Exec(query, taxi.LicensePlate, taxi.Brand, taxi.Body)
	if err != nil {
		return fmt.Errorf("r.db.Exec err: %w ", err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return fmt.Errorf("res.LastInsertId() err: %w ", err)
	}

	taxi.Id = int(id)

	return nil
}

func (r *TaxiMySQL) Get(id int) (*models.TaxiMySql, error) {
	var taxi models.TaxiMySql
	query := "SELECT * FROM taxi WHERE id=?"

	rows, err := r.db.Queryx(query, id)
	if err != nil {
		return nil, fmt.Errorf("r.db.Queryx err: %w ", err)
	}

	for rows.Next() {
		if err := rows.StructScan(&taxi); err != nil {
			return nil, fmt.Errorf("rows.StructScan err: %w ", err)
		}
	}

	return &taxi, nil
}

func (r *TaxiMySQL) GetList(limit, offset int) ([]models.TaxiMySql, error) {
	query := "SELECT id, license_plate, brand, body FROM taxi ORDER BY id DESC LIMIT ? OFFSET ?"

	rows, err := r.db.Queryx(query, limit, offset)
	if err != nil {
		return nil, fmt.Errorf("r.db.Queryx err: %w ", err)
	}

	var taxiList []models.TaxiMySql
	for rows.Next() {
		var taxi models.TaxiMySql

		if err := rows.StructScan(&taxi); err != nil {
			return nil, fmt.Errorf("rows.StructScan err: %w ", err)
		}

		taxiList = append(taxiList, taxi)
	}

	return taxiList, nil
}

func (r *TaxiMySQL) GetQuantity() (int, error) {
	var count int

	err := r.db.QueryRow("SELECT count(*) FROM taxi").Scan(&count)
	return count, err
}

func (r *TaxiMySQL) Update(taxi *models.TaxiMySql) error {
	query := "UPDATE taxi SET license_plate = ? WHERE id = ?"

	_, err := r.db.Exec(query, taxi.LicensePlate, taxi.Id)
	if err != nil {
		return fmt.Errorf("r.db.Exec err:  %w ", err)
	}

	return nil
}

func (r *TaxiMySQL) Delete(id int) error {
	query := "DELETE FROM taxi WHERE id = ?"

	_, err := r.db.Exec(query, id)
	if err != nil {
		return fmt.Errorf("r.db.Exec err:  %w ", err)
	}

	return nil
}

package repository

import (
	"fmt"
	"taxiapp/internal/models"

	"github.com/jmoiron/sqlx"
)

type TrackMySQL struct {
	db *sqlx.DB
}

func NewTrackMySQL(db *sqlx.DB) *TrackMySQL {
	return &TrackMySQL{db: db}
}

func (r *TrackMySQL) Create(trackInfo *models.TrackInfo) (int, error) {
	query := "INSERT INTO track (orders_id, geo_path) VALUES (?, ?)"

	res, err := r.db.Exec(query, trackInfo.OrderId, trackInfo.Coordinates)
	if err != nil {
		return 0, fmt.Errorf("r.db.Exec err: %w ", err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("res.LastInsertId() err: %w ", err)
	}

	return int(id), nil
}

func (r *TrackMySQL) GetById(id int) (*models.Track, error) {
	var track models.Track
	query := "SELECT id, geo_path, created_at FROM track WHERE id=?"

	rows, err := r.db.Queryx(query, id)
	if err != nil {
		return nil, fmt.Errorf("r.db.Queryx err: %w ", err)
	}

	for rows.Next() {
		if err := rows.StructScan(&track); err != nil {
			return nil, fmt.Errorf("rows.StructScan err: %w ", err)
		}
	}

	return &track, nil
}

func (r *TrackMySQL) GetByOrderId(orderId, limit, offset int) ([]models.Track, error) {
	var params []interface{}

	q := "SELECT id, geo_path, created_at FROM track WHERE orders_id=? order by id DESC "
	params = append(params, orderId)

	if limit > 0 {
		q = q + "LIMIT ? "
		params = append(params, limit)
	}
	if offset > 0 {
		q = q + "OFFSET ? "
		params = append(params, offset)
	}

	rows, err := r.db.Queryx(q, params...)
	if err != nil {
		return nil, fmt.Errorf("r.db.Queryx err: %w ", err)
	}

	var orderTrackList []models.Track
	for rows.Next() {
		var orderTrack models.Track

		if err := rows.StructScan(&orderTrack); err != nil {
			return nil, fmt.Errorf("rows.StructScan err: %w ", err)
		}

		orderTrackList = append(orderTrackList, orderTrack)
	}

	return orderTrackList, nil
}

func (r *TrackMySQL) GetLastByOrderId(id int) (*models.Track, error) {
	var track models.Track
	query := "SELECT id, geo_path, created_at FROM track WHERE orders_id=?"

	rows, err := r.db.Queryx(query, id)
	if err != nil {
		return nil, fmt.Errorf("r.db.Queryx err: %w ", err)
	}

	for rows.Next() {
		if err := rows.StructScan(&track); err != nil {
			return nil, fmt.Errorf("rows.StructScan err: %w ", err)
		}
	}

	return &track, nil
}

func (r *TrackMySQL) GetQuantity(orderId int) (int, error) {
	var count int

	err := r.db.QueryRow("SELECT count(*) FROM track WHERE orders_id=?", orderId).Scan(&count)
	return count, err
}

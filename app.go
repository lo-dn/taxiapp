package app

import (
	"context"
	_ "database/sql"
	"fmt"
	"taxiapp/config"
	"taxiapp/internal/delivery/amqp"
	"taxiapp/internal/delivery/http"
	"taxiapp/internal/delivery/http/handler"
	"taxiapp/internal/delivery/http/routes"
	"taxiapp/internal/models"
	"taxiapp/internal/repository"
	"taxiapp/internal/service"
	"taxiapp/pkg/db"
	"taxiapp/pkg/es"
)

func Run(cfg *config.Config) error {
	ctx := context.Background()

	// Init DB
	conn, err := db.Init(cfg.DB)
	if err != nil {
		return fmt.Errorf("db.Init failed: %w", err)
	}

	// Init ES
	esClient, err := es.NewElasticSearch(&cfg.Elastic)
	if err != nil {
		return fmt.Errorf("es.NewElasticSearch failed: %w", err)
	}

	// Init domain deps
	serv := models.Server{
		DB:   conn.DB,
		Conf: cfg,
	}

	repo, err := repository.NewRepository(&ctx, conn.DB, esClient)
	if err != nil {
		return fmt.Errorf("NewRepository failed: %w", err)
	}
	services := service.NewService(repo)
	handlers := handler.NewHandler(services, &cfg.Auth)

	// Init rabbit listeners
	if err := amqp.Init(services, &serv); err != nil {
		return fmt.Errorf("amqp.Init failed: %w", err)
	}

	// Run http server
	if err := http.Start(&serv, routes.InitRoutes(&serv, handlers)); err != nil {
		return fmt.Errorf("http.Start failed: %w", err)
	}

	return nil
}

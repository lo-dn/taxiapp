run:
	docker-compose up --remove-orphans --build

run_taxi:
	go run cmd/taxi/main.go


run_simulator:
	go run cmd/simulator/main.go -delay=1 -workers=1 -track-size=5

help_simulator:
	go run cmd/simulator/main.go -h

swag:
	swag init -g cmd/taxi/main.go

test:
	go test --short -coverprofile=cover.out -v ./...

# Testing Vars
export TEST_CONTAINER_NAME=test_container
export TEST_DB_HOST=localhost
export TEST_DB_PORT=13306
export TEST_DB_NAME=test_db
export TEST_DB_USER=db_user
export TEST_DB_PASSWORD=db_user_pass
export TEST_DB_PASSWORD_ROOT=db_root

test.integration:
	docker run --rm -h $$TEST_DB_HOST -p $$TEST_DB_PORT:3306 --name $$TEST_CONTAINER_NAME -e MYSQL_DATABASE=$$TEST_DB_NAME -e MYSQL_ROOT_PASSWORD=$$TEST_DB_PASSWORD -e MYSQL_USER=$$TEST_DB_USER -e MYSQL_PASSWORD=$$TEST_DB_PASSWORD -e MYSQL_ROOT_PASSWORD=$$TEST_DB_PASSWORD_ROOT -d mysql:latest
	timeout 15 >nul

	GIN_MODE=release go test -v ./tests/
	docker stop $$TEST_CONTAINER_NAME


db_link = "db_user:db_user_pass@/app_db?parseTime=true"

migrate-status:
	goose -dir "migrations" mysql $(db_link) status

migrate-up:
	goose -dir "migrations" mysql $(db_link) up

migrate-down:
	goose -dir "migrations" mysql $(db_link) down



# temporarily
lint:
	gofumpt -w -s ./taxiapp-lozovoi/..
	golangci-lint run --fix


# :construction: Work in progress.
#lint_docker_compose_file = "./development/golangci_lint/docker-compose.yml"
#
#lint-build:
#	@docker-compose --file=$(lint_docker_compose_file) build -q
#
#lint-check:
#	@docker-compose --file=$(lint_docker_compose_file) run --rm taxiapp-lozovoi golangci-lint run \
# 		&& echo "✔️  checked without errors" \
# 		|| echo "☢️  code style issues found"
#
#
#lint-fix:
#	@docker-compose --file=$(lint_docker_compose_file) run --rm taxiapp-lozovoi golangci-lint run --fix \
#		&& echo "✔️  fixed without errors" \
#		|| (echo "⚠️️  you need to fix above issues manually" && exit 1)
#	@echo "⚠️️ run \"make lint-check\" again to check what did not fix yet"